//
//  ViewController.swift
//  NSSOSwiftDemo
//
//  Created by Pankaj Verma on 30/06/17.
//  Copyright © 2017 Pankaj Verma. All rights reserved.
//

import UIKit
import FBSDKLoginKit
class ViewController: UIViewController,GIDSignInDelegate, GIDSignInUIDelegate{
    @IBOutlet weak var emailOrMobile: UITextField!
    @IBOutlet weak var otpOrPassword: UITextField!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
//    @IBAction func initialiseSDK(_ sender: Any)
//    {
//        let projSetting = ["settings" : "Primary settings of TIL SDK",
//                           "nsso" : ["name":"Native SSO",
//                                     "key":"key value"]
//            ] as [String : Any];
//        let tilsdkConfiguration:TILSDK_Configuration =   TILSDK_Configuration.init(settings: projSetting);
//        
//        tilsdkConfiguration.use(ssoIntegrationFactory.instance())
//        
//        TILSDK_Core.setup(with: tilsdkConfiguration)
//        TILSDK_Core.printIntegrations()
//        
//        let loginManager = TILSDK_Core.sharedTILSDK()
//        loginManager?.nSSOSetup(forChannel: "gaana", siteId: "5995f55f00c9d5ce945c778ed96a76f2", teamId: "RB3CUQ8JTM", completion:
//            {
//                (info, err) in
//                
//        })
//    }

   
    @IBAction func sendOTP(_ sender: Any)
    {
                                TILSDK_Core.sharedTILSDK().nSSOsendLoginOtp(onEmail: "", mobile: emailOrMobile.text, success:
                                   {
                                       print("Otp sent")
                                    
                                    let msg = "Otp sent"
                                    self.showAlert(msg: msg)
                              })
                                {
                                    (err) in
                                    
                                    let msg = "code:\((err! as NSError).code) message:\((err! as NSError).localizedDescription)"
                                    self.showAlert(msg: msg)
        }
    }
    
    @IBAction func login(_ sender: Any)
    {
        TILSDK_Core.sharedTILSDK().nSSOverifyLoginOtpPassword(otpOrPassword.text, email: "", mobile: emailOrMobile.text, success:
            {
                let msg = "login successful"
                self.showAlert(msg: msg)
        }, failure: { (err) in
            let msg = "code:\((err! as NSError).code) message:\((err! as NSError).localizedDescription)"
            self.showAlert(msg: msg)
        })
        
    }
    @IBAction func facebookLogin(_ sender: Any)
    {
        /* Through NativeSSO */
        let fbLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["public_profile","email"], from: self.presentedViewController) { (result, err) in
            if(err != nil)
            {
                fbLoginManager.logOut()
                return;
            }
            if(result?.isCancelled)!
            {
                return;
            }
            
            let si = SSOSocialInfo()
            si.accessToken = result!.token.tokenString
            si.oauthId = result!.token.userID
            
            
            (NSSOCrossAppLoginManager.sharedLoginManager() as! NSSOCrossAppLoginManager).performSocialActivity(SSOSocialActivityOptions.facebookLogin, using: si, success: { 
                //login successful
                                    let msg = "login successful"
                                    self.showAlert(msg: msg)
            }, failure: { (err) in
                let msg = "code:\((err! as NSError).code) message:\((err! as NSError).localizedDescription)"
                               self.showAlert(msg: msg)
            })
        }
        
      /* Through TIL SDK */
        //login options
        // var option = SSOSocialActivityOptions.facebookLogin.rawValue;
//            TILSDK_Core.sharedTILSDK().nSSOperformSocialActivity(
//                &option
//                , usingSocialInfo:si
//                , success:
//                {
//                    //login successful
//                    let msg = "login successful"
//                    self.showAlert(msg: msg)
//            })
//            {
//                (err) in
//                
//                let msg = "code:\((err! as NSError).code) message:\((err! as NSError).localizedDescription)"
//                self.showAlert(msg: msg)
//            }
//            
//        }

        
    }
    @IBAction func googleLogin(_ sender: Any)
    {
//        [GIDSignIn sharedInstance].uiDelegate = vc;
//        [GIDSignIn sharedInstance].delegate = self;
//        privateGoogleLoginDelegate = vc;
//        [[GIDSignIn sharedInstance] signIn];
        GIDSignIn.sharedInstance().uiDelegate = self;
        GIDSignIn.sharedInstance().delegate = self;
        GIDSignIn.sharedInstance().signIn()
    }
    @IBAction func getUserDetails(_ sender: Any)
    {
        TILSDK_Core.sharedTILSDK().nSSOgetUserDetails { (userObj, err) in
            if(err==nil){
            let user = userObj as! SSOUserDetails
                
                
                
           var msg = ""
                if let firstName = user.firstName { msg = msg+"firstName: "+firstName}
                if let lastName = user.lastName {msg = msg+"\n lastName:"+lastName}
                if let ssoid = user.ssoid {msg = msg+"\n ssoid"+ssoid}
                if let dp = user.dp {msg = msg+"\n dp:"+dp}
                if let dob = user.dob {msg = msg+", dob:"+dob}
                if let mobileList = user.mobileList {msg = msg+"\n mobileList:"+mobileList.description}
                if let emailList = user.emailList {msg = msg+"\n emailList:"+emailList.description}
                if let fbConnected = user.fbConnected {msg = msg+"\n fbConnected: "+String(fbConnected)}
                if let gpConnected = user.gpConnected {msg = msg+"\n gpConnected:"+String(gpConnected)}
                if let city = user.city {msg = msg+"\n city:"+city}

                self.showAlert(msg: msg)

            }
            else{
                
                let msg = "code:\((err! as NSError).code) message:\((err! as NSError).localizedDescription)"
                self.showAlert(msg: msg)
            }
        }
    }
    @IBAction func signout(_ sender: Any)
    {
        TILSDK_Core.sharedTILSDK().nSSOsignOutUser({
            self.showAlert(msg: "logout successfully ")
        }) { (err) in
            let msg = "code:\((err! as NSError).code) message:\((err! as NSError).localizedDescription)"
            self.showAlert(msg: msg)
        }
        
    }
    @IBAction func getAppSession(_ sender: Any)
    {
     TILSDK_Core.sharedTILSDK().nSSOgetSSOAppSession { (session, err) in
        let ssoSession = session as! SSOSession
        let msg = "ssec:\(ssoSession.ssec!) ticketId:\(ssoSession.ticketId!)"
        self.showAlert(msg: msg)
        }
    }
    @IBAction func getGlobalSession(_ sender: Any)
    {
        TILSDK_Core.sharedTILSDK().nSSOgetSSOGlobalSession(withUserDataEnabled: false, completion: { (gSession, err) in
            if err == nil {
            if let ssoGSession = gSession  {
                
            NSLog((ssoGSession as! SSOSession).ssec!)
            NSLog((ssoGSession as! SSOSession).ticketId!)
            }
            }
            else
            {
                let msg = "code:\((err! as NSError).code) message:\((err! as NSError).localizedDescription)"
                self.showAlert(msg: msg)
            }
        })
    }

    @IBAction func copyGlobalSession(_ sender: Any)
    {
        TILSDK_Core.sharedTILSDK().nSSOcopySSOGlobalSessionToApp { (info, err) in
            if(err == nil)
            {
                print("global session copied  ")
            }
            else
            {
                let msg = "code:\((err! as NSError).code) message:\((err! as NSError).localizedDescription)"
                self.showAlert(msg: msg)
            }
        }
    }
   
    func showAlert(msg:String) -> Void
    {
        let alert = UIAlertController(title: "Alert", message: msg, preferredStyle: UIAlertControllerStyle.alert)
       alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
  
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!)
    {
        if(error == nil)
        {
        let si = SSOSocialInfo()
        si.accessToken = user.authentication.accessToken  // not idToken!
        si.oauthId = user.userID
        
        
        (NSSOCrossAppLoginManager.sharedLoginManager() as! NSSOCrossAppLoginManager).performSocialActivity(SSOSocialActivityOptions.googleLogin, using: si, success: {
            //login successful
            let msg = "login successful"
            self.showAlert(msg: msg)
        }, failure: { (err) in
            let msg = "code:\((err! as NSError).code) message:\((err! as NSError).localizedDescription)"
            self.showAlert(msg: msg)
        })
        }
        else
        {  //error in Google
            let msg = "code:\((error! as NSError).code) message:\((error! as NSError).localizedDescription)"
            self.showAlert(msg: msg)

        
        }
        
    }
    
    
}

