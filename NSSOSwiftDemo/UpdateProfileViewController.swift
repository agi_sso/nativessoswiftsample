//
//  UpdateProfileViewController.swift
//  NSSOSwiftDemo
//
//  Created by Anupam Singh1 on 06/07/17.
//  Copyright © 2017 Pankaj Verma. All rights reserved.
//

import UIKit

class UpdateProfileViewController: UIViewController {

    @IBOutlet weak var first_name: UITextField!
    @IBOutlet weak var last_name: UITextField!
    @IBOutlet weak var dob: UITextField!
    @IBOutlet weak var gender: UITextField!
    @IBOutlet weak var city: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func getDOB(_ sender: UITextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.date
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(datePickerValueChanged), for: UIControlEvents.valueChanged)
    }

    func datePickerValueChanged(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.timeStyle = DateFormatter.Style.none
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dob.text = dateFormatter.string(from: sender.date)
        
    }
    
    @IBAction func updateProfile(_ sender: Any) {
        let userD = SSOUserUpdates()
        userD.firstName = self.first_name.text!
        userD.lastName = self.last_name.text!
        userD.dob = self.dob.text!
        userD.gender = self.gender.text!
        userD.city = self.city.text!
        
        TILSDK_Core.sharedTILSDK().nSSOupdateUserDetails(userD, success: { (userObj) in
            var user = SSOUserUpdates()
            user = userObj as! SSOUserUpdates
            var msg = ""
            msg = String(format: "%@%@%@\n%@%@\n%@%@\n%@%@\n%@%@",  "Profile Updated Successfully\n", "firstName: ",user.firstName!,"lastname : " , user.lastName!,"dob : " , user.dob! , "gender + " , user.gender! , "city : " , user.city!)
            self.showAlert(msg: msg)
        }) { (err) in
            let msg = "code:\((err! as NSError).code) message:\((err! as NSError).localizedDescription)"
            self.showAlert(msg: msg)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func showAlert(msg:String) -> Void
    {
        let alert = UIAlertController(title: "Alert", message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)

    }
    

}
