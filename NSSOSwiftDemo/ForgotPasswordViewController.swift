//
//  ForgotPasswordViewController.swift
//  NSSOSwiftDemo
//
//  Created by Anupam Singh1 on 06/07/17.
//  Copyright © 2017 Pankaj Verma. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController {

    @IBOutlet weak var email_or_mobile: UITextField!
    @IBOutlet weak var otp_or_password: UITextField!
    @IBOutlet weak var new_password: UITextField!
    @IBOutlet weak var confirm_new_password: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func sendFPOtpMobile(_ sender: Any) {
        TILSDK_Core.sharedTILSDK().nSSOgetForgotPasswordOTP(forEmail: "", mobile: email_or_mobile.text!, success: {
            let msg = "Otp Sent on Mobile : " + self.email_or_mobile.text!
            self.showAlert(msg: msg)
        }) { (err) in
            let msg = "code:\((err! as NSError).code) message:\((err! as NSError).localizedDescription)"
            self.showAlert(msg: msg)
        }
    }
    
    @IBAction func resendFPOtpMobile(_ sender: Any) {
        TILSDK_Core.sharedTILSDK().nSSOresendForgotPasswordOTP(forEmail: "", mobile: email_or_mobile.text!, success: {
            let msg = "Otp resent on Mobile : " + self.email_or_mobile.text!
            self.showAlert(msg: msg)
        }) { (err) in
            let msg = "code:\((err! as NSError).code) message:\((err! as NSError).localizedDescription)"
            self.showAlert(msg: msg)
        }
    }

    
    @IBAction func verifyOtpMobile(_ sender: Any) {
        TILSDK_Core.sharedTILSDK().nSSOverifyForgotPassword(forEmail: "", mobile: email_or_mobile.text!, otp: otp_or_password.text!, password: new_password.text!, confirmPassword: confirm_new_password.text!, success: { 
            let msg = "Password Changed Successfully."
            self.showAlert(msg: msg)
        }) { (err) in
            let msg = "code:\((err! as NSError).code) message:\((err! as NSError).localizedDescription)"
            self.showAlert(msg: msg)
        }
    }
    
    @IBAction func sendFPOtpEmail(_ sender: Any) {
        TILSDK_Core.sharedTILSDK().nSSOgetForgotPasswordOTP(forEmail: email_or_mobile.text!, mobile: "", success: {
            let msg = "Otp Sent on Email : " + self.email_or_mobile.text!
            self.showAlert(msg: msg)
        }) { (err) in
            let msg = "code:\((err! as NSError).code) message:\((err! as NSError).localizedDescription)"
            self.showAlert(msg: msg)
        }
    }
    
    @IBAction func resendFPOtpEmail(_ sender: Any) {
        TILSDK_Core.sharedTILSDK().nSSOresendForgotPasswordOTP(forEmail: email_or_mobile.text!, mobile: "", success: {
            let msg = "Otp resent on Email : " + self.email_or_mobile.text!
            self.showAlert(msg: msg)
        }) { (err) in
            let msg = "code:\((err! as NSError).code) message:\((err! as NSError).localizedDescription)"
            self.showAlert(msg: msg)
        }
    }
    
    @IBAction func verifyFPOtpEmail(_ sender: Any) {
        TILSDK_Core.sharedTILSDK().nSSOverifyForgotPassword(forEmail: email_or_mobile.text!, mobile: "", otp: otp_or_password.text!, password: new_password.text!, confirmPassword: confirm_new_password.text!, success: {
            let msg = "Password Changed Successfully."
            self.showAlert(msg: msg)
        }) { (err) in
            let msg = "code:\((err! as NSError).code) message:\((err! as NSError).localizedDescription)"
            self.showAlert(msg: msg)
        }
    }
    
    func showAlert(msg:String) -> Void
    {
        let alert = UIAlertController(title: "Alert", message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}
