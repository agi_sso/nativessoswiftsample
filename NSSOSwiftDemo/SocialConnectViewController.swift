//
//  SocialConnectViewController.swift
//  NSSOSwiftDemo
//
//  Created by Anupam Singh1 on 10/07/17.
//  Copyright © 2017 Pankaj Verma. All rights reserved.
//

import UIKit
import FBSDKLoginKit


class SocialConnectViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func connectFb(_ sender: Any) {
        let fbLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["public_profile","email"], from: self.presentedViewController) { (result, err) in
            if(err != nil)
            {
                fbLoginManager.logOut()
                return;
            }
            if(result?.isCancelled)!
            {
                return;
            }
            
            let si = SSOSocialInfo()
            si.accessToken = result!.token.tokenString
            si.oauthId = result!.token.userID
            
            
            (NSSOCrossAppLoginManager.sharedLoginManager() as! NSSOCrossAppLoginManager).performSocialActivity(SSOSocialActivityOptions.facebookLink, using: si, success: {
                //login successful
                let msg = "login successful"
                self.showAlert(msg: msg)
            }, failure: { (err) in
                let msg = "code:\((err! as NSError).code) message:\((err! as NSError).localizedDescription)"
                self.showAlert(msg: msg)
            })
        }
    }

    @IBAction func connectGp(_ sender: Any) {
        
    }
    
    @IBAction func delinkFb(_ sender: Any) {
        
    }
    
    @IBAction func delinkGp(_ sender: Any) {
        
    }
    
    
    func showAlert(msg:String) -> Void
    {
        let alert = UIAlertController(title: "Alert", message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}
