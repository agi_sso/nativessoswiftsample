//
//  ChangePasswordViewController.swift
//  NSSOSwiftDemo
//
//  Created by Anupam Singh1 on 06/07/17.
//  Copyright © 2017 Pankaj Verma. All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewController {

    @IBOutlet weak var old_password: UITextField!
    @IBOutlet weak var new_password: UITextField!
    @IBOutlet weak var confirm_new_password: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func changePassword(_ sender: Any) {
        
        TILSDK_Core.sharedTILSDK().nSSOchangePassword(old_password.text!, newPassword: new_password.text!, confirmPassword: confirm_new_password.text!, success: {
            let msg = "Password Changed Successfully"
            self.showAlert(msg: msg)
        }) { (err) in
            let msg = "code:\((err! as NSError).code) message:\((err! as NSError).localizedDescription)"
            self.showAlert(msg: msg)
        }
        
    }

    func showAlert(msg:String) -> Void
    {
        let alert = UIAlertController(title: "Alert", message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}
