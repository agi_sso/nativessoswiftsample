//
//  RegisterUserViewController.swift
//  NSSOSwiftDemo
//
//  Created by Pankaj Verma on 04/07/17.
//  Copyright © 2017 Pankaj Verma. All rights reserved.
//

import UIKit

class RegisterUserViewController: UIViewController {
    @IBOutlet weak var mobile: UITextField!

    @IBOutlet weak var email: UITextField!
    
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var otp: UITextField!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }


    @IBAction func signUp(_ sender: UIButton) {
        //In full signup all fields and one of email/mobile are mandatory
        
        let user = SSOSignupUser()
        user.setName(name.text)
        user.setEmail(email.text)
        user.setMobile(mobile.text)
        user.setPassword(password.text)
        user.setGender("M") //for female set "F"
        
        (NSSOCrossAppLoginManager.sharedLoginManager() as! NSSOCrossAppLoginManager).performSignupActivity(SSOSignupOptions.fullSignup, for: user, success: { 
            let msg = "verify otp"
            self.showAlert(msg: msg)
            
        }) { (err) in
            let msg = "code:\((err! as NSError).code) message:\((err! as NSError).localizedDescription)"
            self.showAlert(msg: msg)
            
        }

        
    }
    @IBAction func onlyMobileSignup(_ sender: UIButton)
    {
        // only mobile and password are mandatory
        let user = SSOSignupUser()
      //  user.setName(name.text)
      //  user.setEmail(email.text)
        user.setMobile(mobile.text)
        user.setPassword(password.text)
       // user.setGender("M") //for female set "F"
        
        (NSSOCrossAppLoginManager.sharedLoginManager() as! NSSOCrossAppLoginManager).performSignupActivity(SSOSignupOptions.onlyMobileSignup, for: user, success: {
            let msg = "verify otp"
            self.showAlert(msg: msg)
            
        }) { (err) in
            let msg = "code:\((err! as NSError).code) message:\((err! as NSError).localizedDescription)"
            self.showAlert(msg: msg)
            
        }

    }
    
    @IBAction func verifySignupOtpOnMobile(_ sender: UIButton)
    {
       (NSSOCrossAppLoginManager.sharedLoginManager() as! NSSOCrossAppLoginManager).verfiySignUpOTP(otp.text!, email: "", mobile: mobile.text!, success: {
        let msg = "Signup successful "
        self.showAlert(msg: msg)
       }) { (err) in
        let msg = "code:\((err! as NSError).code) message:\((err! as NSError).localizedDescription)"
        self.showAlert(msg: msg)

        }
        
    }
  
    @IBAction func verifySignupOtpOnEmail(_ sender: UIButton)
    {
        (NSSOCrossAppLoginManager.sharedLoginManager() as! NSSOCrossAppLoginManager).verfiySignUpOTP(otp.text!, email: email.text!, mobile: "", success: {
            let msg = "Signup successful "
            self.showAlert(msg: msg)
        }) { (err) in
            let msg = "code:\((err! as NSError).code) message:\((err! as NSError).localizedDescription)"
            self.showAlert(msg: msg)
            
        }

    }
    
    @IBAction func resendSignupOtpOnMobile(_ sender: Any)
    {
        (NSSOCrossAppLoginManager.sharedLoginManager() as! NSSOCrossAppLoginManager).resendSignUpOtp(forEmail: "", mobile: mobile.text!, success: {
            let msg = "verify otp"
            self.showAlert(msg: msg)
        }) { (err) in
            let msg = "code:\((err! as NSError).code) message:\((err! as NSError).localizedDescription)"
            self.showAlert(msg: msg)
            
        }
   
    }
    @IBAction func resendSignupOtpOnEmail(_ sender: UIButton)
    {
        (NSSOCrossAppLoginManager.sharedLoginManager() as! NSSOCrossAppLoginManager).resendSignUpOtp(forEmail: email.text!, mobile: "", success: {
            let msg = "verify otp"
            self.showAlert(msg: msg)
        }) { (err) in
            let msg = "code:\((err! as NSError).code) message:\((err! as NSError).localizedDescription)"
            self.showAlert(msg: msg)
            
        }
    }
    
    func showAlert(msg:String) -> Void
    {
        let alert = UIAlertController(title: "Alert", message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func close(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
}
