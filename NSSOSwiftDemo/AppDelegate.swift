//
//  AppDelegate.swift
//  NSSOSwiftDemo
//
//  Created by Pankaj Verma on 30/06/17.
//  Copyright © 2017 Pankaj Verma. All rights reserved.
//

import UIKit
import NativeSSOLogin
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn
import TILSSOIntegration

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
    
        //facebook
        FBSDKAppEvents.activateApp();
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        //Google
        GIDSignIn.sharedInstance().clientID = "532845592563-9ljoa3tib3urov3vagih2kdp4u5ebagc.apps.googleusercontent.com"
        
        let projSetting = ["settings" : "Primary settings of TIL SDK",
                                  "nsso" : ["name":"Native SSO",
                                            "key":"key value"]
                                  ] as [String : Any];
        let tilsdkConfiguration:TILSDK_Configuration =   TILSDK_Configuration.init(settings: projSetting);
        
        tilsdkConfiguration.use(ssoIntegrationFactory.instance())
        
        TILSDK_Core.setup(with: tilsdkConfiguration)
        TILSDK_Core.printIntegrations()
        
    let loginManager = TILSDK_Core.sharedTILSDK()
        loginManager?.nSSOSetup(forChannel: "gaana", siteId: "5995f55f00c9d5ce945c778ed96a76f2", teamId: "RB3CUQ8JTM", completion:
            {
                (info, err) in
            

                
        })
        
        
        return true
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url as URL!, sourceApplication: sourceApplication, annotation: annotation) ||
            GIDSignIn.sharedInstance().handle(url, sourceApplication: sourceApplication, annotation: annotation)
    }
}

