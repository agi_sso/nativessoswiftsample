#ifdef __OBJC__
#import <UIKit/UIKit.h>
#endif

#import "TILIntegration.h"
#import "TILIntegrationFactory.h"
#import "TILSDK_Core.h"

FOUNDATION_EXPORT double TILIOSSDKVersionNumber;
FOUNDATION_EXPORT const unsigned char TILIOSSDKVersionString[];

