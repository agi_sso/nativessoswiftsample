#ifdef __OBJC__
#import <UIKit/UIKit.h>
#endif

#import "NSSOCrossAppLoginManager.h"
#import "NSSODownloadManager.h"
#import "NSSOGlobal.h"
#import "NSSOUrlRequestManager.h"
#import "SSOBasicUserDetails.h"
#import "SSOError.h"
#import "SSOForgotPassword.h"
#import "SSOHeaders.h"
#import "SSOIdentifiers.h"
#import "SSOKeychain.h"
#import "SSOResponseModel.h"
#import "SSOSession.h"
#import "SSOSignupUser.h"
#import "SSOSocialInfo.h"
#import "SSOUserDetails.h"
#import "SSOUserStatus.h"
#import "SSOUserUpdates.h"

FOUNDATION_EXPORT double NativeSSOLoginVersionNumber;
FOUNDATION_EXPORT const unsigned char NativeSSOLoginVersionString[];

