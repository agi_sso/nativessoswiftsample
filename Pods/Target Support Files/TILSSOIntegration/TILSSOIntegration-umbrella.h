#ifdef __OBJC__
#import <UIKit/UIKit.h>
#endif

#import "ssoIntegration.h"
#import "ssoIntegrationFactory.h"

FOUNDATION_EXPORT double TILSSOIntegrationVersionNumber;
FOUNDATION_EXPORT const unsigned char TILSSOIntegrationVersionString[];

