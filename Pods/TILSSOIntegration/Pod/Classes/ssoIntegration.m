//
//  ssoIntegration.m
//  tilsdk-ios
//
//  Created by Amit Kumar12 on 07/02/17.
//  Copyright © 2017 Amit Kumar12. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ssoIntegration.h"

@implementation ssoIntegration
-(instancetype)initWithSettings:(NSDictionary *)settings
{
    if (self = [super init]) {
        self.settings = settings;
         self.ssoLoginManager = [NSSOCrossAppLoginManager sharedLoginManager];
    }
    return self;
}
-(instancetype)initWithSettings:(NSDictionary *)settings andssoLoginManager:(NSSOCrossAppLoginManager *)ssoLoginManager
{
    if (self = [super init]) {
        self.settings = settings;
        self.ssoLoginManager = ssoLoginManager;
    }
    return self;
 }

static id NilOnNull(id object)
{
    if (object==[NSNull null]) {
        return nil;
    }
    else
        return object ;
}

//MARK:- Initialization
/*
 Initializes SDK with channel, site id, team id and it makes sure if tgId is present in the App keychain.
 If not search in Shared Keychain and copy it to App Keychain otherwise get it from sso server.
 Without tgId login is not possible.
 */

-(void)ssoSetupForChannel:(NSDictionary*)parameters
{
    [self.ssoLoginManager ssoSetupForChannel:parameters[@"channel"] siteId:parameters[@"siteid"] teamId:parameters[@"teamId"] completion:parameters[@"completion"]];
}

#pragma mark Login via Facebook or Google
/*
 User can login so sso via Facebook or Google plus.
 App will ask user for his basic profile info and email.
 User will be loged in after success.
 */
//-(void)loginViaFacebookOnViewController:(UIViewController *)vc
//                                success:(void(^)())success
//                                failure:(void(^)(NSString *error_message))failure;
//-(void)loginViaGoogleOnViewController:(UIViewController *)vc
//                             delegate:(id)obj;


//-(void)loginUsingSocialInfo:(NSDictionary *)parameters
//{
//    @try {
//         [self.ssoLoginManager loginUsingSocialInfo:parameters[@"site"] forSocilaNetwork:parameters[@"site"] success:parameters[@"success"] failure:parameters[@"failure"]];
//    } @catch (NSException *exception) {
//        NSLog(@"loginUsingSocialInfo : @%",exception.description);
//    }
//}
//-(void)ssologinUsingSocialInfo:(NSMutableDictionary *)info
//           forSocilaNetwork:(NSString *)site
//                    success:(void(^)())success
//                    failure:(void(^)(NSString *error_message))failure;

//#pragma mark Login via email or mobile
/*
 User can login via their registered email or mobile.
 If user know their password they can login with that along with registered email/mobile or
 they can request a login OTP on one of their registered email/mobile.
 User will be loged inn after successfully verified their OTP or Password.
 */
//-(void)sendLoginOtpOnMobile:(NSDictionary *)parameters
//{
//    [self.ssoLoginManager sendLoginOtpOnMobile:parameters[@"mobile"] success:parameters[@"success"] failure:parameters[@"failure"]];
//}

//-(void)sendLoginOtpOnMobile:(NSString *)mobile
//                    success:(void(^)())success
//                    failure:(void(^)(NSString *error_message))failure;
//

-(void)sendLoginOtpOnEmail:(NSDictionary *)parameters
{
    [self.ssoLoginManager sendLoginOtpOnEmail:parameters[@"email"] mobile:parameters[@"mobile"] success:parameters[@"success"] failure:parameters[@"failure"]];
}

//-(void)sendLoginOtpOnEmail:(NSString *)email
//                   success:(void(^)())success
//                   failure:(void(^)(NSString *error_message))failure;
//

-(void)resendLoginOTPOnEmail:(NSDictionary *)parameters
{
    [self.ssoLoginManager resendLoginOTPOnEmail:parameters[@"email"] mobile:parameters[@"mobile"] success:parameters[@"success"] failure:parameters[@"failure"]];
}

//-(void)resendLoginOTPOnEmail:(NSString *)email
//                     success:(void(^)())success
//                     failure:(void(^)(NSString *error_message))failure;
//
//-(void)resendLoginOTPOnMobile:(NSDictionary *)parameters
//{
//    [self.ssoLoginManager resendLoginOTPOnMobile:parameters[@"mobile"] success:parameters[@"success"] failure:parameters[@"failure"]];
//}
//-(void)resendLoginOTPOnMobile:(NSString *)mobile
//                      success:(void(^)())success
//                      failure:(void(^)(NSString *error_message))failure;
//

//-(void)verifyLoginOtpPasswordMobile:(NSDictionary *)parameters
//{
//    [self.ssoLoginManager verifyLoginOtpPassword:parameters[@"password"] forMobile:parameters[@"mobile"] success:parameters[@"success"] failure:parameters[@"failure"]];
//}

//-(void)verifyLoginOtpPassword:(NSString *)password
//                    forMobile:(NSString *)mobile
//                      success:(void(^)())success
//                      failure:(void(^)(NSString *error_message))failure;

-(void)verifyLoginOtpPassword:(NSDictionary *)parameters
{

    [self.ssoLoginManager verifyLoginOtpPassword:parameters[@"password"] email:parameters[@"email"] mobile:parameters[@"mobile"] success:parameters[@"success"] failure:parameters[@"failure"]];
}

//-(void)verifyLoginOtpPassword:(NSString *)password
//                     forEmail:(NSString *)email
//                      success:(void(^)())success
//                      failure:(void(^)(NSString *error_message))failure;
//
//

-(void)verfiySignUpOTP:(NSDictionary*) parameters
{
    [self.ssoLoginManager verfiySignUpOTP:parameters[@"otp"] email:parameters[@"email"] mobile:parameters[@"mobile"] success:parameters[@"success"] failure:parameters[@"failure"]];
}


//-(void)verfiySignUpOTP:(NSString *)otp
//              forEmail:(NSString *)email
//               success:(void(^)())success
//               failure:(void(^)(NSString *error_message))failure

//-(void)verfiySignUpOTPMobile:(NSDictionary*) parameters
//{
//    [self.ssoLoginManager verfiySignUpOTP:parameters[@"otp"] forMobile:parameters[@"mobile"] success:parameters[@"success"] failure:parameters[@"failure"]];
//}


//-(void)verfiySignUpOTP:(NSString *)otp
//              forMobile:(NSString *)email
//               success:(void(^)())success
//               failure:(void(^)(NSString *error_message))failure

//#pragma mark New user registration or signup

-(void)registerUser:(NSDictionary*) parameters
{
    bool isSendOfferEnabled = parameters[@"isSendOfferEnabled"];
    [self.ssoLoginManager registerUser:parameters[@"name"] mobile:parameters[@"mobile"] email:parameters[@"email"] password:parameters[@"password"] gender:parameters[@"gender"] isSendOfferEnabled:isSendOfferEnabled success:parameters[@"success"] failure:parameters[@"failure"]];
}

//-(void)registerUser:(NSString *)name
//             mobile:(NSString *)mobile
//              email:(NSString *)email
//           password:(NSString *)password
//             gender:(NSString *)gender
// isSendOfferEnabled:(BOOL)isSendOfferEnabled
//            success:(void(^)())success
//            failure:(void(^)(NSString *error_message))failure

//#pragma mark signOutUser
-(void)signOutUser:(NSDictionary *)parameters
{
    [self.ssoLoginManager signOutUser:parameters[@"success"] failure:parameters[@"failure"]];
}
//-(void)signOutUser:(void(^)())success
//           failure:(void(^)(NSString *error_message))failure;
//
//#pragma mark getUserDetails

-(void)getUserDetails:(NSDictionary *)parameters
{
    [self.ssoLoginManager getUserDetails:parameters[@"success"] failure:parameters[@"failure"]];
}

//-(void)getUserDetails:(void(^)(NSDictionary *info))success
//              failure:(void(^)(NSString *error_message))failure;
//
//#pragma mark change password
///*
// User can change their password.
// New password must be different than previous three passwords.
// */
-(void)changePassword:(NSDictionary *)parameters
{
    [self.ssoLoginManager changePassword:parameters[@"oldPassword"] newPassword:parameters[@"newPassword"] confirmPassword:parameters[@"confirmPassword"] success:parameters[@"success"] failure:parameters[@"failure"]];
}
//-(void)changePassword:(NSString *)oldPassword
//          newPassword:(NSString *)newPassword
//      confirmPassword:(NSString *)confirmPassword
//              success:(void(^)())success
//              failure:(void(^)(NSString *error_message))failure;
//
//
//#pragma mark recover password
///* If user have forgetten their password.
// User can request for forgot password OTP on one of their registered email/mobile and with this OTP they can provide new password.
// New password must not match previous three passwords.
// New password will be set on successful verification.
// */

//-(void)getForgotPasswordOTPForMobile:(NSDictionary *)parameters
//{
//    [self.ssoLoginManager getForgotPasswordOTPForMobile:parameters[@"mobile"] success:parameters[@"success"] failure:parameters[@"failure"]];
//}
//-(void)getForgotPasswordOTPForMobile:(NSString *)mobile
//                             success:(void(^)())success
//                             failure:(void(^)(NSString *error_message))failure;
//

-(void)getForgotPasswordOTPForEmail:(NSDictionary *)parameters
{
    [self.ssoLoginManager getForgotPasswordOTPForEmail:parameters[@"email"] mobile:parameters[@"mobile"]success:parameters[@"success"] failure:parameters[@"failure"]];
}

//-(void)getForgotPasswordOTPForEmail:(NSString *)email
//                            success:(void(^)())success
//                            failure:(void(^)(NSString *error_message))failure;
//
//-(void)resendForgotPasswordOTPForMobile:(NSDictionary *)parameters
//{
//    [self.ssoLoginManager resendForgotPasswordOTPForMobile:parameters[@"mobile"] success:parameters[@"success"] failure:parameters[@"failure"]];
//}
//-(void)resendForgotPasswordOTPForMobile:(NSString *)mobile
//                                success:(void(^)())success
//                                failure:(void(^)(NSString *error_message))failure;
//

-(void)resendForgotPasswordOTPForEmail:(NSDictionary *)parameters
{
    [self.ssoLoginManager resendForgotPasswordOTPForEmail:parameters[@"email"] mobile:parameters[@"mobile"] success:parameters[@"success"] failure:parameters[@"failure"]];
}

//-(void)resendForgotPasswordOTPForEmail:(NSString *)email
//                               success:(void(^)())success
//                               failure:(void(^)(NSString *error_message))failure;
//

//-(void)resendSignUpOtpForMobile:(NSDictionary *)parameters
//{
//    [self.ssoLoginManager resendSignUpOtpForMobile:parameters[@"mobile"] success:parameters[@"success"] failure:parameters[@"failure"]];
//}
//-(void)resendSignUpOtpForMobile:(NSString *)mobile
//                        success:(void(^)())success
//                        failure:(void(^)(NSString *error_message))failure

-(void)resendSignUpOtpForEmail:(NSDictionary *)parameters
{
    [self.ssoLoginManager resendSignUpOtpForEmail:parameters[@"email"] mobile:parameters[@"mobile"] success:parameters[@"success"] failure:parameters[@"failure"]];
}
//-(void)resendSignUpOtpForMobile:(NSString *)mobile
//                        success:(void(^)())success
//                        failure:(void(^)(NSString *error_message))failure

//-(void)verifyForgotPasswordForMobile:(NSDictionary *)parameters
//{
//    [self.ssoLoginManager verifyForgotPasswordForMobile:parameters[@"mobile"] otp:parameters[@"otp"] password:parameters[@"password"] confirmPassword:parameters[@"confirmPassword"] success:parameters[@"success"] failure:parameters[@"failure"]];
//}

//-(void)verifyForgotPasswordForMobile:(NSString *)mobile
//                                 otp:(NSString *)otp
//                            password:(NSString *)password
//                     confirmPassword:(NSString *)confirmPassword
//                             success:(void(^)())success failure:(void(^)(NSString *error_message))failure;
//

-(void)verifyForgotPasswordForEmail:(NSDictionary *)parameters
{
    [self.ssoLoginManager verifyForgotPasswordForEmail:parameters[@"email"] mobile:parameters[@"mobile"] otp:parameters[@"otp"] password:parameters[@"password"] confirmPassword:parameters[@"confirmPassword"] success:parameters[@"success"] failure:parameters[@"failure"]];
}


//-(void)verifyForgotPasswordForEmail:(NSString *)email
//                                otp:(NSString *)otp
//                           password:(NSString *)password
//                    confirmPassword:(NSString *)confirmPassword
//                            success:(void(^)())success
//                            failure:(void(^)(NSString *error_message))failure;
//

//#pragma mark Add Email or Mobile

-(void)addAlternateEmail:(NSDictionary *)parameters
{
    [self.ssoLoginManager addAlternateEmail:parameters[@"email"] success:parameters[@"success"] failure:parameters[@"failure"]];
}


//-(void)addAlternateEmail:(NSString *)email
//                 success:(void(^)())success
//                 failure:(void(^)(NSString *error_message))failure;

-(void)updateMobile:(NSDictionary *)parameters
{
    [self.ssoLoginManager updateMobile:parameters[@"mobile"] success:parameters[@"success"] failure:parameters[@"failure"]];
}

//-(void)updateMobile: (NSString *) mobile
//            success:(void(^)())success
//            failure:(void(^)(NSString *error_message))failure;

-(void)verifyAddAlternateEmailOtp:(NSDictionary *)parameters
{
    [self.ssoLoginManager verifyAddAlternateEmailOtp:parameters[@"otp"] forEmail:parameters[@"email"] success:parameters[@"success"] failure:parameters[@"failure"]];
}
//-(void)verifyAddAlternateEmailOtp:(NSString *)otp
//                         forEmail:(NSString *)email
//                          success:(void(^)())success
//                          failure:(void(^)(NSString *error_message))failure;

-(void)verifyUpdateMobileOtp:(NSDictionary *)parameters
{
    [self.ssoLoginManager verifyUpdateMobileOtp:parameters[@"otp"] forMobile:parameters[@"mobile"] success:parameters[@"success"] failure:parameters[@"failure"]];
}

//-(void)verifyUpdateMobileOtp:(NSString *)otp
//                   forMobile:(NSString *)mobile
//                     success:(void(^)())success
//                     failure:(void(^)(NSString *error_message))failure;


//MARK:- version v2.0
/*
 Apps which are using their own login have to migrate their login session to SSO session so that users which are already login in App will also be in login state after update(integration of SSO SDK).
 This is one time call.
 After migration done App can remove its own session.
 */
-(void)migrateCurrentSessionToAppHavingTicketId:(NSDictionary *)parameters
{
    [self.ssoLoginManager migrateCurrentSessionToAppHavingTicketId:parameters[@"ticketId"] completion:parameters[@"completion"]];
}
//-(void)migrateCurrentSessionToAppHavingTicketId:(NSString *)ticketId
//                                     completion:(void(^)(NSDictionary *info,NSString * error_message))completion;
//



//temp: make below two api private

-(void)getAppSessionOnCompletion:(void(^)(NSDictionary *info,NSError *error))completion
{
    [self.ssoLoginManager getAppSessionOnCompletion:completion];
}
//-(NSDictionary *)getAppSession;




/*
 This is Cross app login and works silentely.
 If User is new and Global session exist, he/she can contine with global session.
 Global session will be copied to App with new refreshed ticketId.
 Note: Existing user who are in logout state before integration of This SDK in the App will be treated as new user and will be (may be )silentely login after this update. They are requested to logout if not happy or want to login with different account :P
 */
-(void)getGlobalSessionOnCompletion:(void(^)(NSDictionary *info,NSError * error))completion;
{
    [self.ssoLoginManager getGlobalSessionOnCompletion:completion];
}
-(void)copySSOGlobalSessionToAppOnCompletion:(void(^)(NSDictionary *info,NSError * error))completion
{
    [self.ssoLoginManager copySSOGlobalSessionToAppOnCompletion:completion];
}

//-(BOOL)copySSOGlobalSessionToApp;



/*
 Passive login using Facebook.
 If user is new and no global session found(i.e. Cross App login not possible) it will be asked to continue with their Facebook account.
 */
//-(void)doPassiveLogin;



/*
 This method checks if User is comming for the first time.
 If YES it will check if Cross App login possible if YES executes doCrossAppLogin block
 Else execute doPassiveLogin block;
 */
//-(void)loginSilentlyIfFirstLoginWithGlobalSession:(void(^)(NSDictionary * info))doCrossAppLogin
//                                  doPassiveLogin:(void(^)())doPassiveLogin;


//-(void)getSilentLoginOptions:(void(^)(NSDictionary *loginOptions))completion
//{
//    @try {
//         NSDictionary *returnValue = [self.ssoLoginManager getSilentLoginOptions];
//        completion(returnValue);
//    } @catch (NSException *exception) {
//        NSLog(@"getSilentLoginOptions : @%",exception.description);
//    }
//
//}

//-(NSDictionary *)getSilentLoginOptions;

//-(void)isNewAppLogin:(void(^)(BOOL value))completion
//{
//    @try {
//        BOOL returnValue = [self.ssoLoginManager isNewAppLogin];
//        completion(returnValue);
//    } @catch (NSException *exception) {
//        NSLog(@"isNewAppLogin : @%",exception.description);
//    }
//
//}
//-(BOOL)isNewAppLogin;

-(void)renewTicket:(NSDictionary *)parameters   // Return type modifiend please check
{
    [self.ssoLoginManager renewTicket:parameters[@"success"] failure:parameters[@"failure"]];
}

/*
 Will return ticketId of loged in user.
 */


-(void)updateFirstName:(NSDictionary *)parameters
{
    [self.ssoLoginManager updateFirstName:parameters[@"firstName"] lastName:parameters[@"lastName"] dob:parameters[@"dob"] gender:parameters[@"gender"] success:parameters[@"success"] failure:parameters[@"failure"]];
}


//-(void)updateFirstName:(NSString *)firstName
//              lastName:(NSString *)lastName
//                   dob:(NSString *)dob
//                gender:(NSString *)gender
//               success:(void(^)())success
//               failure:(void(^)(NSString *error_message))failure;
//temp

-(void)deleteAllSSOCredentialsFromApp:(NSDictionary *)parameters
{
//    [self.ssoLoginManager deleteAllSSOCredentialsFromApp:parameters[@"success"] failure:parameters[@"failure"]];
}

//-(void)deleteAllSSOCredentialsFromApp: (void(^)())success
//                              failure:(void(^)(NSString *error_message))failure;

-(void)deleteGlobalSessionIfAny:(NSDictionary *)parameters
{
//    [self.ssoLoginManager deleteGlobalSessionIfAny:parameters[@"success"] failure:parameters[@"failure"]];
}
//-(void)deleteGlobalSessionIfAny: (void(^)())success
//                        failure:(void(^)(NSString *error_message))failure;

//v2.1
//MARK:- Social link delink
-(void)loginUsingSocialInfo:(NSMutableDictionary *)parameters
{
    [self.ssoLoginManager loginUsingSocialInfo:parameters[@"info"] success:parameters[@"success"] failure:parameters[@"failure"]];

}
-(void)linkSocialAccountUsingInfo:(NSDictionary *)parameters
{
    [self.ssoLoginManager linkSocialAccountUsingInfo:parameters[@"info"] success:parameters[@"success"] failure:parameters[@"failure"]];
}
//-(void)linkSocialAccountUsingInfo:(NSMutableDictionary *)info
//                          success:(void(^)())success
//                          failure:(void(^)(NSString *error_message))failure;
-(void)delinkFacebook:(NSDictionary *)parameters
{
    [self.ssoLoginManager delinkFacebook:parameters[@"success"] failure:parameters[@"failure"]];
}
//-(void)delinkFacebook:(void(^)())success
//              failure:(void(^)(NSString *error_message))failure;
-(void)delinkGoogleplus:(NSDictionary *)parameters
{
    [self.ssoLoginManager delinkGoogleplus:parameters[@"success"] failure:parameters[@"failure"]];
}
//-(void)delinkGoogleplus:(void(^)())success
//                failure:(void(^)(NSString *error_message))failure;
//
//#pragma mark :- socialImageUpload
//
-(void)uploadProfilePicFromSocialUsingInfo:(NSDictionary *)parameters
{
    [self.ssoLoginManager uploadProfilePicFromSocialUsingInfo:parameters[@"info"] success:parameters[@"success"] failure:parameters[@"failure"]];
}
//-(void)uploadProfilePicFromSocialUsingInfo:(NSMutableDictionary *)info
//                                   success:(void(^)())success
//                                   failure:(void(^)(NSString *error_message))failure;
//
////MARK:- Upload Profile Pic
-(void)openPhotoSelectorOnViewController:(NSDictionary *)parameters
{
    [self.ssoLoginManager openPhotoSelectorOnViewController:parameters[@"vc"] success:parameters[@"success"] failure:parameters[@"failure"]];
}

//-(void)openPhotoSelectorOnViewController:(UIViewController *)vc
//                                 success:(void(^)())success
//                                 failure:(void(^)(NSString *error_message))failure;

// Version 1.0.6

#pragma mark - New Apis 2.2

//getUserDetails: user details will be returned in an object of type SSOUserDetails.
-(void)getUserDetailsOnCompletion:(NSDictionary*)parameters
{
    [self.ssoLoginManager getUserDetailsOnCompletion:NilOnNull(parameters[@"completion"])];
}

// This Api will check if user(email/mobile) is registered with SSO). Status will be returned in an object of type SSOUserStatus.
-(void)getStatusForIdentifier:(NSDictionary* )parameters
{
    
    [self.ssoLoginManager getStatusForIdentifier:NilOnNull(parameters[@"identifier"]) success:NilOnNull(parameters[@"success"]) failure:NilOnNull(parameters[@"failure"])];
}


//update user details : first name, last name, gender(M or F), dob,city. Leave the field blank which you do not want to update.
// Updated details will be returned in an object of type SSOUserStatus
-(void)updateUserDetails:(NSDictionary* )parameters
{
    SSOUserUpdates *userDetails = nil;
    if (NilOnNull(parameters[@"userDetails"])) {
        userDetails = (SSOUserUpdates*)parameters[@"userDetails"]; //Type Conversion
    }
    [self.ssoLoginManager updateUserDetails:userDetails success:NilOnNull(parameters[@"success"]) failure:NilOnNull(parameters[@"failure"])];
}





//Login Sessions
//migrate existing session
-(void) createAppSessionForTicketId:(NSDictionary*)parameters
{
    [self.ssoLoginManager createAppSessionForTicketId:parameters[@"ticketId"] completion:parameters[@"completion"]];
}

//global session:  if userDataEnabled is true, it will return session + basic info of global user else only global session will be returnd. result will be an object of type SSOSession
-(void)getSSOGlobalSessionWithUserDataEnabled:(NSDictionary*)parameters
{
    Boolean userDataEnabled = [parameters[@"userDataEnabled"] boolValue];
    
    [self.ssoLoginManager getSSOGlobalSessionWithUserDataEnabled:userDataEnabled completion:NilOnNull(parameters[@"completion"])];
}
//app session: result will be an object of type SSOSession
-(void)getSSOAppSessionOnCompletion:(NSDictionary*)parameters
{
    [self.ssoLoginManager getSSOAppSessionOnCompletion:NilOnNull(parameters[@"completion"])];
}




// Login via Facebook,Google or Truecaller; link/d-link Facebook or Google account to SSO; upload profile pic from Gggole or Facebook account
//Note: Truecaller login is in beta phase
//For facebook and Google set oauthId and accessToken
//For Trucaller(only login supported) set payload and signature

-(void)performSocialActivity:(NSDictionary* )parameters
{
    SSOSocialInfo *info = nil;
    if (NilOnNull(parameters[@"info"])) {
        info = (SSOSocialInfo*)parameters[@"info"]; //Type Conversion
    }
    SSOSocialActivityOptions options = [parameters[@"options"] integerValue];
    
    [self.ssoLoginManager performSocialActivity:options usingSocialInfo:info  success:NilOnNull(parameters[@"success"]) failure:NilOnNull(parameters[@"failure"])];
}


//user signup: User can signup by providing their full details or just mobile number and name.
-(void)performSignupActivity:(NSDictionary* )parameters
{
    SSOSignupUser *user = nil;
    if (NilOnNull(parameters[@"user"])) {
        user = (SSOSignupUser*)parameters[@"user"]; //Type Conversion
    }
    SSOSignupOptions options = [parameters[@"options"] integerValue];
    
    [self.ssoLoginManager performSignupActivity:options forUser:user success:NilOnNull(parameters[@"success"]) failure:NilOnNull(parameters[@"failure"])];
}
//profile pic upload from camera/gallery
-(void)performPickUploadActivity:(NSDictionary* )parameters
{
    SSOPickUploadOptions options = [parameters[@"options"] integerValue];
    [self.ssoLoginManager performPickUploadActivity:options onController:NilOnNull(parameters[@"vc"]) startRequest:NilOnNull(parameters[@"uploadStart"]) success:NilOnNull(parameters[@"success"]) failure:NilOnNull(parameters[@"failure"])];

}

@end
