//
//  ssoIntegrationFactory.h
//  tilsdk-ios
//
//  Created by Amit Kumar12 on 07/02/17.
//  Copyright © 2017 Amit Kumar12. All rights reserved.
//

#ifndef ssoIntegrationFactory_h
#define ssoIntegrationFactory_h


#endif /* ssoIntegrationFactory_h */
#import <Foundation/Foundation.h>
#import <TILIOSSDK/TILIntegrationFactory.h>


@interface ssoIntegrationFactory : NSObject <TILIntegrationFactory>

+ (instancetype)instance;

@end
