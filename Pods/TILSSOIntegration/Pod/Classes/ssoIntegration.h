//
//  ssoIntegration.h
//  tilsdk-ios
//
//  Created by Amit Kumar12 on 06/02/17.
//  Copyright © 2017 Amit Kumar12. All rights reserved.
//

#ifndef ssoIntegration_h
#define ssoIntegration_h


#endif /* ssoIntegration_h */
#import <Foundation/Foundation.h>
#import "TILIOSSDK/TILIntegration.h"
#import <NativeSSOLogin/NSSOCrossAppLoginManager.h>

@interface ssoIntegration : NSObject <TILIntegration>

@property (nonatomic, strong) NSDictionary *settings;
@property (nonatomic, strong) NSSOCrossAppLoginManager *ssoLoginManager;

- (instancetype)initWithSettings:(NSDictionary *)settings;
 - (instancetype)initWithSettings:(NSDictionary *)settings andssoLoginManager:(NSSOCrossAppLoginManager *)ssoLoginManager;

//MARK:- Initialization
/*
Initializes SDK with channel, site id, team id and it makes sure if tgId is present in the App keychain.
If not search in Shared Keychain and copy it to App Keychain otherwise get it from sso server.
Without tgId login is not possible.
 */

-(void)ssoSetupForChannel:(NSDictionary *)parameters;
    
#pragma mark Login via Facebook or Google
    /*
     User can login so sso via Facebook or Google plus.
     App will ask user for his basic profile info and email.
     User will be loged in after success.
     */
    //-(void)loginViaFacebookOnViewController:(UIViewController *)vc
    //                                success:(void(^)())success
    //                                failure:(void(^)(NSString *error_message))failure;
    //-(void)loginViaGoogleOnViewController:(UIViewController *)vc
    //                             delegate:(id)obj;

-(void)loginUsingSocialInfo:(NSDictionary *)parameters;
//-(void)loginUsingSocialInfo:(NSMutableDictionary *)info
//           forSocilaNetwork:(NSString *)site
//                    success:(void(^)())success
//                    failure:(void(^)(NSString *error_message))failure;



#pragma mark Login via email or mobile
    /*
     User can login via their registered email or mobile.
     If user know their password they can login with that along with registered email/mobile or
     they can request a login OTP on one of their registered email/mobile.
     User will be loged inn after successfully verified their OTP or Password.
     */
//-(void)sendLoginOtpOnMobile:(NSDictionary *)parameters;

-(void)sendLoginOtpOnEmail:(NSDictionary *)parameters;
    
-(void)resendLoginOTPOnEmail:(NSDictionary *)parameters;
    
//-(void)resendLoginOTPOnMobile:(NSDictionary *)parameters;

//-(void)verifyLoginOtpPasswordMobile:(NSDictionary *)parameters;
-(void)verifyLoginOtpPasswordEmail:(NSDictionary *)parameters;
    

#pragma mark New user registration or signup
-(void)ssoRegisterUser:(NSDictionary *)parameters;

    
#pragma mark signOutUser
-(void)signOutUser:(NSDictionary *)parameters;

#pragma mark getUserDetails
-(void)getUserDetails:(NSDictionary *)parameters;

#pragma mark change password
    /*
     User can change their password.
     New password must be different than previous three passwords.
     */
-(void)changePassword:(NSDictionary *)parameters;


#pragma mark recover password
    /* If user have forgetten their password.
     User can request for forgot password OTP on one of their registered email/mobile and with this OTP they can provide new password.
     New password must not match previous three passwords.
     New password will be set on successful verification.
     */
//-(void)getForgotPasswordOTPForMobile:(NSDictionary *)parameters;

-(void)getForgotPasswordOTPForEmail:(NSDictionary *)parameters;
    
//-(void)resendForgotPasswordOTPForMobile:(NSDictionary *)parameters;

-(void)resendForgotPasswordOTPForEmail:(NSDictionary *)parameters;
    
//-(void)verifyForgotPasswordForMobile:(NSDictionary *)parameters;

-(void)verifyForgotPasswordForEmail:(NSDictionary *)parameters;

#pragma mark Add Email or Mobile

-(void)addAlternateEmail:(NSDictionary *)parameters;
-(void)updateMobile: (NSDictionary *)parameters;
-(void)verifyAddAlternateEmailOtp:(NSDictionary *)parameters;
-(void)verifyUpdateMobileOtp:(NSDictionary *)parameters;

//MARK:- version v2.0
/*
 Apps which are using their own login have to migrate their login session to SSO session so that users which are already login in App will also be in login state after update(integration of SSO SDK).
 This is one time call.
 After migration done App can remove its own session.
 */
-(void)getGlobalSessionOnCompletion:(void(^)(NSDictionary *info,NSError * error))completion;
-(void)migrateCurrentSessionToAppHavingTicketId:(NSDictionary*)parameters;
-(void)getSSOGlobalSession:(void(^)(NSDictionary *getValue))completion;
-(void)getAppSessionOnCompletion:(void(^)(NSDictionary *info,NSError * error))completion;
-(void)copySSOGlobalSessionToAppOnCompletion:(void(^)(NSDictionary *info,NSError * error))completion;
-(void)getSilentLoginOptions:(void(^)(NSDictionary *loginOptions))completion;
-(void)isNewAppLogin:(void(^)(BOOL value))completion;
-(void)renewTicket:(NSDictionary *)parameters;
//-(void)getTicketId:(void(^)(NSString *ticktedId))completion;
-(void)updateFirstName:(NSDictionary*)parameters;
//temp
-(void)deleteAllSSOCredentialsFromApp:(NSDictionary*)parameters;
-(void)deleteGlobalSessionIfAny:(NSDictionary*)parameters;
//v2.1
//MARK:- Social link delink
-(void)loginUsingSocialInfo:(NSDictionary *)parameters;
-(void)linkSocialAccountUsingInfo:(NSDictionary *)parameters;
-(void)delinkFacebook:(NSDictionary *)parameters;
-(void)delinkGoogleplus:(NSDictionary *)parameters;

#pragma mark :- socialImageUpload

-(void)uploadProfilePicFromSocialUsingInfo:(NSDictionary *)parameters;
//MARK:- Upload Profile Pic
-(void)openPhotoSelectorOnViewController:(NSDictionary *)parameters;

#pragma mark - New Apis 2.2

//getUserDetails: user details will be returned in an object of type SSOUserDetails.
-(void)getUserDetailsOnCompletion:(NSDictionary* )parameters;

// This Api will check if user(email/mobile) is registered with SSO). Status will be returned in an object of type SSOUserStatus.
-(void)getStatusForIdentifier:(NSDictionary* )parameters;


//update user details : first name, last name, gender(M or F), dob,city. Leave the field blank which you do not want to update.
// Updated details will be returned in an object of type SSOUserStatus
-(void)updateUserDetails:(NSDictionary* )parameters;





//Login Sessions
-(void) createAppSessionForTicketId:(NSDictionary*)parameters;

//global session:  if userDataEnabled is true, it will return session + basic info of global user else only global session will be returnd. result will be an object of type SSOSession
-(void)getSSOGlobalSessionWithUserDataEnabled:(NSDictionary* )parameters;
//app session: result will be an object of type SSOSession
-(void)getSSOAppSessionOnCompletion:(NSDictionary* )parameters;




// Login via Facebook,Google or Truecaller; link/d-link Facebook or Google account to SSO; upload profile pic from Gggole or Facebook account
//Note: Truecaller login is in beta phase
//For facebook and Google set oauthId and accessToken
//For Trucaller(only login supported) set payload and signature

-(void)performSocialActivity:(NSDictionary* )parameters;


//user signup: User can signup by providing their full details or just mobile number and name.
-(void)performSignupActivity:(NSDictionary* )parameters;

//profile pic upload from camera/gallery
-(void)performPickUploadActivity:(NSDictionary* )parameters;
@end
