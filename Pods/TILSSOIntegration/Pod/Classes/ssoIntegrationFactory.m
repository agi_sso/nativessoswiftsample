//
//  ssoIntegrationFactory.m
//  tilsdk-ios
//
//  Created by Amit Kumar12 on 06/02/17.
//  Copyright © 2017 Amit Kumar12. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ssoIntegrationFactory.h"
#import "TILIntegration.h"
#import "ssoIntegration.h"


@implementation ssoIntegrationFactory

+ (instancetype)instance
{
    static dispatch_once_t once;
    static ssoIntegrationFactory *sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    return self;
}

- (id<TILIntegration>)createWithSettings:(NSDictionary *)settings
{
    return [[ssoIntegration alloc] initWithSettings:settings];
}

- (NSString *)key
{
    return @"nsso";
}

@end
