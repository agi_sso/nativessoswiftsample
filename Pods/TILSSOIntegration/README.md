# SSOIntegration

SSOIntegration is an iOS Integration for TILSDK Core.

## Installation

SSOIntegration is available through [CocoaPods](http://cocoapods.org)

### CocoaPods

please replace your username with you bitbucket id


```ruby
pod "TILSSOIntegration", :git=>'https://bitbucket.org/tilsdk/til_ios_ssointegration.git', :tag=> '0.1.0'
```

## Quickstart
```objective-c
[tilsdkConfiguration use:[ssoIntegrationFactory instance]];
```
