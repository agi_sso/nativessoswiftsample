# TILSDK

TILIOS SDK is an iOS client for TIL IOS SDK Core.

## Installation

TILSDK is available through [CocoaPods](http://cocoapods.org)

### CocoaPods


```ruby
pod "TILIOSSDK", :git=>'https://@bitbucket.org/tilsdk/til_ios_sdk.git', :tag=> '1.0.6'
```
### Documentation
[TILSDK Documentation](http://tilsdk.indiatimes.com/)

## Quickstart
```objective-c
TILSDK_Configuration *tilsdkConfiguration = [TILSDK_Configuration configurationWithSettings:projectSettings];

    [tilsdkConfiguration use:[SampleIntegrationFactory instance]];
    [TILSDK_Core setupWithConfiguration:tilsdkConfiguration];
    [TILSDK_Core printIntegrations];
```

### Integrations Enabled 

1. SSOIntegration
2. DMPIntegration
3. TPIntegration
