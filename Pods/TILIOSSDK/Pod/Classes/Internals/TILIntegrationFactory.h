//
//  TILIntegrationFactory.h
//  tilsdk-ios
//
//  Created by Amit Kumar12 on 24/11/16.
//  Copyright © 2016 Amit Kumar12. All rights reserved.
//

#ifndef TILIntegrationFactory_h
#define TILIntegrationFactory_h


#endif /* TILIntegrationFactory_h */

#import "TILSDK_Core.h"
#import "TILIntegration.h"

@class TILSDK_Core;
@class TILSDK_Configuration;

@protocol TILIntegrationFactory

/**
 * Attempts to create an adapter with the given settings. Returns the adapter if one was created, or null
 * if this factory isn't capable of creating such an adapter.
 */

- (id<TILIntegration>)createWithSettings:(NSDictionary *)settings;

/** The key for which this factory can create an Integration. */
- (NSString *)key;

@end
