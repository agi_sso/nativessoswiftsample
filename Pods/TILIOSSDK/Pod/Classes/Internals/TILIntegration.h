//
//  TILIntegration.h
//  tilsdk-ios
//
//  Created by Amit Kumar12 on 30/11/16.
//  Copyright © 2016 Amit Kumar12. All rights reserved.
//

#ifndef TILIntegration_h
#define TILIntegration_h


#endif /* TILIntegration_h */

#import <Foundation/Foundation.h>
@protocol TILIntegration <NSObject>



@optional


// App Delegate Callbacks

// Callbacks for notifications changes.
// ------------------------------------
- (void)receivedRemoteNotification:(NSDictionary *)userInfo;
- (void)failedToRegisterForRemoteNotificationsWithError:(NSError *)error;
- (void)registeredForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken;
- (void)handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo;

// Callbacks for app state changes
// -------------------------------

- (void)applicationDidFinishLaunching:(NSNotification *)notification;
- (void)applicationDidEnterBackground;
- (void)applicationWillEnterForeground;
- (void)applicationWillTerminate;
- (void)applicationWillResignActive;
- (void)applicationDidBecomeActive;

- (void)continueUserActivity:(NSUserActivity *)activity;
//- (void)openURL:(NSURL *)url options:(NSDictionary *)options;

@end
