//
//  TILSDK_Core.m
//  tilsdk-ios
//
//  Created by Amit Kumar12 on 23/11/16.
//  Copyright © 2016 Amit Kumar12. All rights reserved.
//

#import "TILSDK_Core.h"
#import <Foundation/Foundation.h>
#import "TILIntegrationFactory.h"
#import "TILIntegration.h"

//Global Variables

static TILSDK_Core *sharedInstance = nil;

@interface TILSDK_Configuration ()

@property (nonatomic, copy, readwrite) NSDictionary *settings;
@property (nonatomic, strong, readonly) NSMutableArray *factories;

@end

@implementation TILSDK_Configuration

+ (instancetype)configurationWithSettings:(NSDictionary *)settings
{
    NSAssert(settings!=Nil, @"Settings Can't be Nil.");
    return [[TILSDK_Configuration alloc] initWithSettings:settings];
}

- (instancetype)initWithSettings:(NSDictionary *)settings
{
    if (self = [self init]) {
        self.settings = settings;
    }
    return self;
}

- (instancetype)init
{
    if (self = [super init]) {
        _factories = [NSMutableArray array];
    }
    return self;
}

- (void)use:(id<TILIntegrationFactory>)factory
{
    [self.factories addObject:factory];
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"<%p:%@, %@>", self, self.class, [self dictionaryWithValuesForKeys:@[ @"settings", @"integrations", @"flushAt" ]]];
}
@end

@interface TILSDK_Core()

@property (nonatomic, strong) NSDictionary *cachedSettings;
@property (nonatomic, strong) TILSDK_Configuration *configuration;
@property (nonatomic, strong) NSArray *factories;
@property (nonatomic, strong) NSMutableDictionary *integrations;
@property (nonatomic, strong) NSMutableDictionary *registeredIntegrations;


@end


@implementation TILSDK_Core

+ (void)setupWithConfiguration:(TILSDK_Configuration *)configuration
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] initWithConfiguration:configuration];
        [sharedInstance performInitializeIntegrations];
    });
}


- (instancetype)initWithConfiguration:(TILSDK_Configuration *)configuration
{
    NSCParameterAssert(configuration != nil);

    if (self = [self init]) {
        self.cachedSettings = configuration.settings;
        self.configuration = configuration;
        self.factories = [configuration.factories copy];
        self.integrations = [NSMutableDictionary dictionaryWithCapacity:self.factories.count];
        self.registeredIntegrations = [NSMutableDictionary dictionaryWithCapacity:self.factories.count];

    }
    return self;
}

# pragma mark - Check Integration

-(id<TILIntegration>)checkIntegration:(NSString*)key
{
    @try {
         return self.integrations[key];
    }
    @catch (NSException *exception) {
        NSLog(@"%@", exception.reason);
    }
}

-(void)performInitializeIntegrations
{
    for (int i = 0; i<sharedInstance.factories.count;i++)
    {
        NSDictionary *settings;
        @try {
             settings = sharedInstance.cachedSettings[[sharedInstance.factories[i] key]];
            NSLog(@"Settings for the key : %@",[sharedInstance.factories[i] key]);
        } @catch (NSException *exception) {
            NSLog(@"Settings not found for key : %@",[sharedInstance.factories[i] key]);
        }

        @try {
             [sharedInstance.integrations setObject:[sharedInstance.factories[i] performSelector:@selector(createWithSettings:) withObject:settings] forKey:[sharedInstance.factories[i] key]];

        } @catch (NSException *exception) {
            NSLog(@"Coundn't found selector : createWithSettings");
        }
    }
}

+(NSDictionary *)bundledIntegrations
{
    return sharedInstance.integrations;
}
+ (instancetype)sharedTILSDK  // Class Variable
{
    NSAssert(sharedInstance!=Nil, @"You haven't initialized TILSDK");
    return sharedInstance;
}
+(void)printIntegrations
{
    for (int i = 0; i<sharedInstance.integrations.count;i++)
    {
        NSLog(@"Integrations : %@",sharedInstance.integrations);
    }
}

static id ObjectOrNull(id object)
{
    return object ?: [NSNull null];
}
/*
 *  Times Point Functions
 *
 */

/* To Validate the user */
-(void) tpinitWithChannelCode:(NSString *) pCode subChannelCode:(NSString*)sCode userId:(NSString *) userId deviceId:(NSString *) deviceId ticketId:(NSString*)ticketId andCompletionBlock:(void (^)(NSError* error))completionHandler{

    NSAssert(pCode!=Nil && sCode!=Nil && userId!=Nil && deviceId!=Nil, @"One or more parameters nil");
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"tp"] respondsToSelector:@selector(initWithChannelCode:)];
    if(selectorResponse) {
        [[sharedInstance checkIntegration:@"tp"] performSelector:@selector(initWithChannelCode:)
                                                        withObject:@{
                                                                     @"pCode":pCode,
                                                                     @"sCode":sCode,
                                                                     @"userId":userId,
                                                                     @"ticketId":ticketId,
                                                                     @"deviceId":deviceId,
                                                                     @"completionHandler":completionHandler
                                                                     }];
    }
    else{
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : initWithChannelCode"}];
        completionHandler(error);
    }
}

    /* saves the user activity and send it to server */
-(void) tplogActivityWithCode:(NSString *)code transactionID:(NSString*)transactionID tranactionIdHash:(NSString*)transactionIdHash andCompletionBlock:(void (^)(NSError* error))completionHandler{

    NSAssert(code!=Nil && transactionID!=Nil && transactionIdHash!=Nil, @"One or more parameters nil");
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"tp"] respondsToSelector:@selector(logActivityWithCode:)];
    if(selectorResponse) {
        [[sharedInstance checkIntegration:@"tp"] performSelector:@selector(logActivityWithCode:)
                                                      withObject:@{
                                                                   @"code":code,
                                                                   @"transactionID":transactionID,
                                                                   @"transactionIdHash":transactionIdHash,
                                                                   @"completionHandler":completionHandler
                                                                   }];
    }
    else{
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : logActivityWithCode"}];
        completionHandler(error);
    }
}
-(void) tpsetTransactionId:(NSString*)transactionId{
    NSAssert(transactionId!=Nil, @"transactionId nil");
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"tp"] respondsToSelector:@selector(setTransactionId:)];
    if(selectorResponse) {
        [[sharedInstance checkIntegration:@"tp"] performSelector:@selector(setTransactionId:)
                                                      withObject:@{
                                                                   @"transactionId":transactionId
                                                                   }];
    }
}

    // On successful logout from  app removes all the user defaults of timespoint
-(void) tplogOutWithCompletionBlock:(void (^)(NSError* error))completionHandler{
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"tp"] respondsToSelector:@selector(logOutWithCompletionBlock:)];
    if(selectorResponse) {
        [[sharedInstance checkIntegration:@"tp"] performSelector:@selector(logOutWithCompletionBlock:)
                                                      withObject:@{
                                                                   @"completionHandler":completionHandler
                                                                   }];
    }
    else{
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : logOutWithCompletionBlock"}];
        completionHandler(error);
    }
}

    // activates timer and other functionalities
-(void)tpactivateAppWithCompletionBlock:(void (^)(NSError* error))completionHandler{
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"tp"] respondsToSelector:@selector(activateAppWithCompletionBlock:)];
    if(selectorResponse) {
        [[sharedInstance checkIntegration:@"tp"] performSelector:@selector(activateAppWithCompletionBlock:)
                                                      withObject:@{
                                                                   @"completionHandler":completionHandler
                                                                   }];
    }
    else{
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : activateAppWithCompletionBlock"}];
        completionHandler(error);
    }
}

    // deactivate timer and other functionalities
-(void)tpdeactivateAppWithCompletionBlock:(void (^)(NSError* error))completionHandler{
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"tp"] respondsToSelector:@selector(deactivateAppWithCompletionBlock:)];
    if(selectorResponse) {
        [[sharedInstance checkIntegration:@"tp"] performSelector:@selector(deactivateAppWithCompletionBlock:)
                                                      withObject:@{
                                                                   @"completionHandler":completionHandler
                                                                   }];
    }
    else{
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : deactivateAppWithCompletionBlock"}];
        completionHandler(error);
    }
}

-(void)tpinitBannerView:(UIView*)view WithType:(NSString*)bannerType andCompletionBlock:(void (^)(NSError* error))completionHandler{
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"tp"] respondsToSelector:@selector(initBannerView:)];
    if(selectorResponse) {
        [[sharedInstance checkIntegration:@"tp"] performSelector:@selector(initBannerView:)
                                                      withObject:@{
                                                                   @"view":view,
                                                                   @"bannerType":bannerType,
                                                                   @"completionHandler":completionHandler
                                                                   }];
    }
    else{
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : initBannerView"}];
        completionHandler(error);
    }
}

/*
 *  Native SSO Functions
 *
 */

-(void)nSSOSetupForChannel:(NSString *)channel
                    siteId:(NSString *)siteid
                    teamId:(NSString *)teamId
                    completion:(void(^)(NSDictionary *info,NSError *error))completion {
    NSAssert(channel!=Nil && siteid!=Nil && teamId!=Nil && completion!=Nil, @"One or more parameters nil");
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"nsso"] respondsToSelector:@selector(ssoSetupForChannel:)];

    if(selectorResponse){
        [[sharedInstance checkIntegration:@"nsso"] performSelector:@selector(ssoSetupForChannel:)
                                                        withObject:@{
                                                                     @"channel":channel,
                                                                     @"siteid":siteid,
                                                                     @"teamId":teamId,
                                                                     @"completion":completion
                                                                     }];
    }
    else{
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : ssoSetupForChannel"}];
        completion(Nil, error);
    }
}

#pragma mark Login via email or mobile
/*
 User can login via their registered email or mobile.
 If user know their password they can login with that along with registered email/mobile or
 they can request a login OTP on one of their registered email/mobile.
 User will be loged inn after successfully verified their OTP or Password.
 */

-(void)nSSOsendLoginOtpOnEmail:(NSString *)email
                        mobile:(NSString *)mobile
                       success:(void(^)())success
                       failure:(void(^)(NSError *error))failure
{
    NSAssert(email!=Nil && mobile!=Nil && success!=Nil && failure!=Nil, @"One or more parameters nil");
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"nsso"] respondsToSelector:@selector(sendLoginOtpOnEmail:)];
    if(selectorResponse) {
        [[sharedInstance checkIntegration:@"nsso"] performSelector:@selector(sendLoginOtpOnEmail:)
                                                        withObject:@{
                                                                     @"email":email,
                                                                     @"mobile":mobile,
                                                                     @"success":success,
                                                                     @"failure":failure
                                                                     }];
    }
    else{
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : sendLoginOtpOnEmail"}];
        failure(error);
    }
}

-(void)nSSOresendLoginOTPOnEmail:(NSString *)email
                          mobile:(NSString *)mobile
                         success:(void(^)())success
                         failure:(void(^)(NSError *error))failure
{
    NSAssert(email!=Nil && success!=Nil && failure!=Nil, @"One or more parameters nil");
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"nsso"] respondsToSelector:@selector(resendLoginOTPOnEmail:)];
    if(selectorResponse) {
        [[sharedInstance checkIntegration:@"nsso"] performSelector:@selector(resendLoginOTPOnEmail:)
                                                        withObject:@{
                                                                     @"email":email,
                                                                     @"mobile":mobile,
                                                                     @"success":success,
                                                                     @"failure":failure
                                                                     }];
    }
    else{
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : resendLoginOTPOnEmail"}];
        failure(error);
    }

}

-(void)nSSOverifyLoginOtpPassword:(NSString *)password
                            email:(NSString *)email
                        mobile:(NSString *)mobile
                          success:(void(^)())success
                          failure:(void(^)(NSError *error))failure
{
    NSAssert(password!=Nil && mobile!=Nil && success!=Nil && failure!=Nil, @"One or more parameters nil");
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"nsso"] respondsToSelector:@selector(verifyLoginOtpPassword:)];
    if(selectorResponse) {
        [[sharedInstance checkIntegration:@"nsso"] performSelector: @selector(verifyLoginOtpPassword:)
                                                        withObject:@{
                                                                     @"password":password,
                                                                     @"email":email,
                                                                     @"mobile":mobile,
                                                                     @"success":success,
                                                                     @"failure":failure
                                                                     }];
    }else {
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : verifyLoginOtpPassword"}];
        failure(error);
    }
}

#pragma mark New user registration or signup
/*
 User can provide either mobile or email or both.
 User will provede their gender, choose a password(following standerd) and agree to send offer notifications(YES or NO).
 All fields are mandatory except email/mobile.
 A sign up otp is preferably send on mobile.
 If mobile is not given otp will be send on email.
 User can resend OTP.
 User will be loged in once OTP is successfully verified.
 */
-(void)nSSOregisterUser:(NSString *)name
                 mobile:(NSString *)mobile
                  email:(NSString *)email
               password:(NSString *)password
                 gender:(NSString *)gender
     isSendOfferEnabled:(BOOL)isSendOfferEnabled
                success:(void(^)())success
                failure:(void(^)(NSError *error))failure
{
    NSAssert(name!=Nil && mobile!=Nil && email!= Nil && password!=Nil && gender!=Nil && success!=Nil && failure!=Nil, @"One or more parameters nil");
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"nsso"] respondsToSelector:@selector(registerUser:)];
    if(selectorResponse) {
        [[sharedInstance checkIntegration:@"nsso"] performSelector:@selector(registerUser:)
                                                        withObject:@{
                                                                     @"name":name,
                                                                     @"mobile":mobile,
                                                                     @"email":email,
                                                                     @"password":password,
                                                                     @"gender":gender,
                                                                     @"isSendOfferEnabled":@(isSendOfferEnabled),
                                                                     @"success":success,
                                                                     @"failure":failure
                                                                     }];
    } else {
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : registerUser"}];
        failure(error);
    }
}

-(void)nSSOresendSignUpOtpForEmail:(NSString *)email
                            mobile:(NSString *)mobile
                           success:(void(^)())success
                           failure:(void(^)(NSError *error))failure
{
    NSAssert(email!= Nil && mobile!=Nil && success!=Nil && failure!=Nil, @"One or more parameters nil");
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"nsso"] respondsToSelector:@selector(resendSignUpOtpForEmail:)];
    if(selectorResponse) {
        [[sharedInstance checkIntegration:@"nsso"] performSelector:@selector(resendSignUpOtpForEmail:)
                                                        withObject:@{
                                                                     @"email":email,
                                                                     @"mobile":mobile,
                                                                     @"success":success,
                                                                     @"failure":failure
                                                                     }];
    } else {
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : resendSignUpOtpForEmail"}];
        failure(error);
    }
}

-(void)nSSOverfiySignUpOTP:(NSString *)otp
                  email:(NSString *)email
                    mobile:(NSString *)mobile
                   success:(void(^)())success
                   failure:(void(^)(NSError *error))failure
{
    NSAssert(otp!= Nil && email!= Nil && mobile != Nil && success!=Nil && failure!=Nil, @"One or more parameters nil");
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"nsso"] respondsToSelector:@selector(verfiySignUpOTP:)];
    if(selectorResponse) {
        [[sharedInstance checkIntegration:@"nsso"] performSelector:@selector(verfiySignUpOTP:)
                                                        withObject:@{
                                                                     @"otp":otp,
                                                                     @"email":email,
                                                                     @"mobile":mobile,
                                                                     @"success":success,
                                                                     @"failure":failure
                                                                     }];
    } else {
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : verfiySignUpOTP"}];
        failure(error);
    }
}

#pragma mark signOutUser
/*
 User will be loged out.
 Loging out will delete App session.
 Global session will be deleted only it is same as App session.
 */

-(void)nSSOsignOutUser:(void(^)())success
               failure:(void(^)(NSError *error))failure
{
    NSAssert(success!=Nil && failure!=Nil, @"One or more parameters nil");
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"nsso"] respondsToSelector:@selector(signOutUser:)];
    if(selectorResponse)  {
        [[sharedInstance checkIntegration:@"nsso"] performSelector:@selector(signOutUser:)
                                                        withObject:@{
                                                                     @"success":success,
                                                                     @"failure":failure
                                                                     }];
    } else {
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : signOutUser"}];
        failure(error);
    }
}

#pragma mark getUserDetails
/*
 User details ccontains date of birth(dob), profile pic url(dp), user's registered email list(emailList),
 First Name, Last Name, Gender, user's registered email list(mobileList), primary email, ssoId
 and whether Facebbok or Google plus account is connected with SSO or not.
 */
-(void)nSSOgetUserDetails:(void(^)(NSDictionary *info))success
                  failure:(void(^)(NSError *error))failure
{
    NSAssert(success!=Nil && failure!=Nil, @"One or more parameters nil");
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"nsso"] respondsToSelector:@selector(getUserDetails:)];
    if(selectorResponse)  {
        [[sharedInstance checkIntegration:@"nsso"] performSelector:@selector(getUserDetails:)
                                                        withObject:@{
                                                                     @"success":success,
                                                                     @"failure":failure
                                                                     }];
    } else {
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : getUserDetails"}];
        failure(error);
    }
}


#pragma mark change password
/*
 User can change their password.
 New password must be different than previous three passwords.
 */
-(void)nSSOchangePassword:(NSString *)oldPassword
              newPassword:(NSString *)newPassword
          confirmPassword:(NSString *)confirmPassword
                  success:(void(^)())success
                  failure:(void(^)(NSError *error))failure
{
    NSAssert(oldPassword!= Nil && newPassword!= Nil && confirmPassword!= Nil && success!=Nil && failure!=Nil, @"One or more parameters nil");
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"nsso"] respondsToSelector:@selector(changePassword:)];
    if(selectorResponse)  {
        [[sharedInstance checkIntegration:@"nsso"] performSelector:@selector(changePassword:)
                                                        withObject:@{
                                                                     @"oldPassword":oldPassword,
                                                                     @"newPassword":newPassword,
                                                                     @"confirmPassword":confirmPassword,
                                                                     @"success":success,
                                                                     @"failure":failure
                                                                     }];
    } else {
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : changePassword"}];
        failure(error);
    }
}


#pragma mark recover password
/* If user have forgetten their password.
 User can request for forgot password OTP on one of their registered email/mobile and with this OTP they can provide new password.
 New password must not match previous three passwords.
 New password will be set on successful verification.
 */
-(void)nSSOgetForgotPasswordOTPForMobile:(NSString *)mobile
                                 success:(void(^)())success
                                 failure:(void(^)(NSError *error))failure
{
    NSAssert(mobile!= Nil && success!=Nil && failure!=Nil, @"One or more parameters nil");
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"nsso"] respondsToSelector:@selector(getForgotPasswordOTPForMobile:)];
    if(selectorResponse)  {
        [[sharedInstance checkIntegration:@"nsso"] performSelector:@selector(getForgotPasswordOTPForMobile:)
                                                        withObject:@{
                                                                     @"mobile":mobile,
                                                                     @"success":success,
                                                                     @"failure":failure
                                                                     }];
    } else {
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : getForgotPasswordOTPForMobile"}];
        failure(error);
    }
}

-(void)nSSOgetForgotPasswordOTPForEmail:(NSString *)email
                                 mobile:(NSString *)mobile
                                success:(void(^)())success
                                failure:(void(^)(NSError *error))failure
{
    NSAssert(email!= Nil && mobile!= Nil && success!=Nil && failure!=Nil, @"One or more parameters nil");
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"nsso"] respondsToSelector:@selector(getForgotPasswordOTPForEmail:)];
    if(selectorResponse)  {
        [[sharedInstance checkIntegration:@"nsso"] performSelector:@selector(getForgotPasswordOTPForEmail:)
                                                        withObject:@{
                                                                     @"email":email,
                                                                     @"mobile":mobile,
                                                                     @"success":success,
                                                                     @"failure":failure
                                                                     }];
    } else {
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : getForgotPasswordOTPForEmail"}];
        failure(error);
    }
}

-(void)nSSOresendForgotPasswordOTPForEmail:(NSString *)email
                                    mobile:(NSString *)mobile
                                   success:(void(^)())success
                                   failure:(void(^)(NSError *error))failure
{
    NSAssert(email!= Nil && mobile!= Nil && success!=Nil && failure!=Nil, @"One or more parameters nil");
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"nsso"] respondsToSelector:@selector(resendForgotPasswordOTPForEmail:)];
    if(selectorResponse)  {
        [[sharedInstance checkIntegration:@"nsso"] performSelector:@selector(resendForgotPasswordOTPForEmail:)
                                                        withObject:@{
                                                                     @"email":email,
                                                                     @"mobile":mobile,
                                                                     @"success":success,
                                                                     @"failure":failure
                                                                     }];
    } else {
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : resendForgotPasswordOTPForEmail"}];
        failure(error);
    }
}

-(void)nSSOverifyForgotPasswordForEmail:(NSString *)email
                                 mobile:(NSString *)mobile
                                    otp:(NSString *)otp
                               password:(NSString *)password
                        confirmPassword:(NSString *)confirmPassword
                                success:(void(^)())success
                                failure:(void(^)(NSError *error))failure
{
    NSAssert(email!= Nil && mobile!= Nil && otp!= Nil && password!= Nil && confirmPassword!= Nil && success!=Nil && failure!=Nil, @"One or more parameters nil");
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"nsso"] respondsToSelector:@selector(verifyForgotPasswordForEmail:)];
    if(selectorResponse)  {
        [[sharedInstance checkIntegration:@"nsso"] performSelector:@selector(verifyForgotPasswordForEmail:)
                                                        withObject:@{
                                                                     @"email":email,
                                                                     @"mobile":mobile,
                                                                     @"otp":otp,
                                                                     @"password":password,
                                                                     @"confirmPassword":confirmPassword,
                                                                     @"success":success,
                                                                     @"failure":failure
                                                                     }];
    } else {
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : verifyForgotPasswordForEmail"}];
        failure(error);
    }
}

#pragma mark Add Email or Mobile
-(void)nSSOaddAlternateEmail:(NSString *)email
                     success:(void(^)())success
                     failure:(void(^)(NSError *error))failure
{
    NSAssert(email!= Nil && success!=Nil && failure!=Nil, @"One or more parameters nil");
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"nsso"] respondsToSelector:@selector(addAlternateEmail:)];
    if(selectorResponse)  {
        [[sharedInstance checkIntegration:@"nsso"] performSelector:@selector(addAlternateEmail:)
                                                        withObject:@{
                                                                     @"email":email,
                                                                     @"success":success,
                                                                     @"failure":failure
                                                                     }];
    } else {
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : addAlternateEmail"}];
        failure(error);
    }
}
-(void)nSSOupdateMobile: (NSString *) mobile
                success:(void(^)())success
                failure:(void(^)(NSError *error))failure
{
    NSAssert(mobile!= Nil && success!=Nil && failure!=Nil, @"One or more parameters nil");
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"nsso"] respondsToSelector:@selector(updateMobile:)];
    if(selectorResponse)  {
        [[sharedInstance checkIntegration:@"nsso"] performSelector:@selector(updateMobile:)
                                                        withObject:@{
                                                                     @"mobile":mobile,
                                                                     @"success":success,
                                                                     @"failure":failure
                                                                     }];
    } else {
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : updateMobile"}];
        failure(error);
    }
}
-(void)nSSOverifyAddAlternateEmailOtp:(NSString *)otp
                             forEmail:(NSString *)email
                              success:(void(^)())success
                              failure:(void(^)(NSError *error))failure
{
    NSAssert(otp!= Nil && email!= Nil && success!=Nil && failure!=Nil, @"One or more parameters nil");
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"nsso"] respondsToSelector:@selector(verifyAddAlternateEmailOtp:)];
    if(selectorResponse)  {
        [[sharedInstance checkIntegration:@"nsso"] performSelector:@selector(verifyAddAlternateEmailOtp:)
                                                        withObject:@{
                                                                     @"otp":otp,
                                                                     @"email":email,
                                                                     @"success":success,
                                                                     @"failure":failure
                                                                     }];
    } else {
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : verifyAddAlternateEmailOtp"}];
        failure(error);
    }
}
-(void)nSSOverifyUpdateMobileOtp:(NSString *)otp
                       forMobile:(NSString *)mobile
                         success:(void(^)())success
                         failure:(void(^)(NSError *error))failure
{
    NSAssert(otp!= Nil && mobile!= Nil && success!=Nil && failure!=Nil, @"One or more parameters nil");
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"nsso"] respondsToSelector:@selector(verifyUpdateMobileOtp:)];
    if(selectorResponse)  {
        [[sharedInstance checkIntegration:@"nsso"] performSelector: @selector(verifyUpdateMobileOtp:)
                                                        withObject:@{
                                                                     @"otp":otp,
                                                                     @"mobile":mobile,
                                                                     @"success":success,
                                                                     @"failure":failure
                                                                     }];
    } else {
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : verifyUpdateMobileOtp"}];
        failure(error);
    }
}

//MARK:- version v2.0

-(void)nSSOmigrateCurrentSessionToAppHavingTicketId:(NSString *)ticketId
                                     completion:(void(^)(NSDictionary *info,NSError *error))completion
{
    NSAssert(ticketId!=Nil && completion!=Nil, @"One or more parameters nil");
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"nsso"] respondsToSelector:@selector(migrateCurrentSessionToAppHavingTicketId:)];
    if(selectorResponse)  {
        [[sharedInstance checkIntegration:@"nsso"] performSelector:@selector(migrateCurrentSessionToAppHavingTicketId:)
                                                        withObject:@{
                                                                     @"ticketId":ticketId,
                                                                     @"completion":completion
                                                                     }];
    }else {
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : migrateCurrentSessionToAppHavingTicketId"}];
        completion(Nil,error);
    }

}

//temp: make below two api private
-(void)nSSOgetGlobalSessionOnCompletion:(void(^)(NSDictionary *info,NSError * error))completion
{
    NSAssert(completion!=Nil, @"One or more parameters nil");
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"nsso"] respondsToSelector:@selector(getGlobalSessionOnCompletion:)];
    if(selectorResponse)  {
        [[sharedInstance checkIntegration:@"nsso"] performSelector:@selector(getGlobalSessionOnCompletion:)
                                                        withObject:completion];
    } else {
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : getGlobalSessionOnCompletion"}];
        completion(Nil,error);
    }
}
-(void)nSSOgetSSOGlobalSession:(void(^)(NSDictionary *getValue,NSError *error))completion
{
    NSAssert(completion!=Nil, @"One or more parameters nil");
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"nsso"] respondsToSelector:@selector(getSSOGlobalSession:)];
    if(selectorResponse) {
        [[sharedInstance checkIntegration:@"nsso"] performSelector:@selector(getSSOGlobalSession:)
                                                        withObject:completion];
    } else {
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : getSSOGlobalSession"}];
        completion(Nil,error);
    }

}

-(void)nSSOgetAppSessionOnCompletion:(void(^)(NSDictionary *info,NSError * error))completion
{
    NSAssert(completion!=Nil, @"One or more parameters nil");
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"nsso"] respondsToSelector:@selector(getAppSessionOnCompletion:)];
    if(selectorResponse)  {
        [[sharedInstance checkIntegration:@"nsso"] performSelector:@selector(getAppSessionOnCompletion:)
                                                        withObject:completion];
    } else {
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : getAppSessionOnCompletion"}];
        completion(Nil,error);
    }

}

/*
 This is Cross app login and works silentely.
 If User is new and Global session exist, he/she can contine with global session.
 Global session will be copied to App with new refreshed ticketId.
 Note: Existing user who are in logout state before integration of This SDK in the App will be treated as new user and will be (may be )silentely login after this update. They are requested to logout if not happy or want to login with different account :P
 */

-(void)nSSOcopySSOGlobalSessionToAppOnCompletion:(void(^)(NSDictionary *info,NSError * error))completion
{
    NSAssert(completion!=Nil, @"One or more parameters nil");
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"nsso"] respondsToSelector:@selector(copySSOGlobalSessionToAppOnCompletion:)];
    if(selectorResponse){
        [[sharedInstance checkIntegration:@"nsso"] performSelector:@selector(copySSOGlobalSessionToAppOnCompletion:)
                                                        withObject:completion];
    } else {
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : copySSOGlobalSessionToAppOnCompletion"}];
        completion(Nil,error);
    }

}

/*
 This method checks if User is comming for the first time.
 If YES it will check if Cross App login possible if YES executes doCrossAppLogin block
 Else execute doPassiveLogin block;
 */
//-(void)loginSilentlyIfFirstLoginWithGlobalSession:(void(^)(NSDictionary * info))doCrossAppLogin
//                                  doPassiveLogin:(void(^)())doPassiveLogin;


-(void)nSSOgetSilentLoginOptions:(void(^)(NSDictionary *loginOptions, NSError *error))completion
{
    NSAssert(completion!=Nil, @"One or more parameters nil");
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"nsso"] respondsToSelector:@selector(getSilentLoginOptions:)];
    if(selectorResponse)  {
        [[sharedInstance checkIntegration:@"nsso"] performSelector:@selector(getSilentLoginOptions:)
                                                        withObject:completion];
    } else {
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : getSilentLoginOptions"}];
        completion(Nil,error);
    }
}


-(void)nSSOisNewAppLogin:(void(^)(BOOL value,NSError *error))completion
{
    NSAssert(completion!=Nil, @"One or more parameters nil");
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"nsso"] respondsToSelector:@selector(isNewAppLogin:)];
    if(selectorResponse)  {
        [[sharedInstance checkIntegration:@"nsso"] performSelector:@selector(isNewAppLogin:)
                                                        withObject:completion];
    } else {
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : isNewAppLogin"}];
        completion(Nil,error);
    }

}

/*
 * This API will reset the life time of ticket to 30 days from now of the log in user .
 */

-(void)nSSOrenewTicket:(void(^)(void))success
             failure:(void(^)(NSError *error))failure
{
    NSAssert(success!=Nil && failure!=Nil, @"One or more parameters nil");
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"nsso"] respondsToSelector:@selector(renewTicket:)];
    if(selectorResponse)  {
        [[sharedInstance checkIntegration:@"nsso"] performSelector:@selector(renewTicket:)
                                                        withObject:@{
                                                                     @"success":success,
                                                                     @"failure":failure
                                                                     }];
    } else {
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : renewTicket"}];
        failure(error);
    }
}

/*
 User can update their name, dob and gender. The field which you do not want to update give NULL value or blank.
 */

-(void)nSSOupdateFirstName:(NSString *)firstName
              lastName:(NSString *)lastName
                   dob:(NSString *)dob
                gender:(NSString *)gender
               success:(void(^)())success
               failure:(void(^)(NSError *error))failure
{
    NSAssert(firstName!= Nil && lastName!= Nil && dob!= Nil && gender!= Nil && success!=Nil && failure!=Nil, @"One or more parameters nil");
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"nsso"] respondsToSelector:@selector(updateFirstName:)];
    if(selectorResponse)  {
        [[sharedInstance checkIntegration:@"nsso"] performSelector: @selector(updateFirstName:)
                                                        withObject:@{
                                                                    @"firstName":firstName,
                                                                     @"lastName":lastName,
                                                                     @"dob":dob,
                                                                     @"gender":gender,
                                                                     @"success":success,
                                                                     @"failure":failure
                                                                     }];
    } else {
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : updateFirstName"}];
        failure(error);
    }
}

-(void)nSSOdeleteAllSSOCredentialsFromApp: (void(^)())success
                              failure:(void(^)(NSError *error))failure
{
    NSAssert(success!=Nil && failure!=Nil, @"One or more parameters nil");
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"nsso"] respondsToSelector:@selector(deleteAllSSOCredentialsFromApp:)];
    if(selectorResponse)  {
        [[sharedInstance checkIntegration:@"nsso"] performSelector: @selector(deleteAllSSOCredentialsFromApp:)
                                                        withObject:@{
                                                                     @"success":success,
                                                                     @"failure":failure
                                                                     }];
    } else {
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : deleteAllSSOCredentialsFromApp"}];
        failure(error);
    }

}


-(void)nSSOdeleteGlobalSessionIfAny:(void(^)())success
                        failure:(void(^)(NSError *error))failure
{
    NSAssert(success!=Nil && failure!=Nil, @"One or more parameters nil");
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"nsso"] respondsToSelector:@selector(deleteGlobalSessionIfAny:)];
    if(selectorResponse)  {
        [[sharedInstance checkIntegration:@"nsso"] performSelector: @selector(deleteGlobalSessionIfAny:)
                                                        withObject:@{
                                                                     @"success":success,
                                                                     @"failure":failure
                                                                     }];
    } else {
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : deleteGlobalSessionIfAny"}];
        failure(error);
    }
}

//v2.1
//MARK:- Social link delink
-(void)nSSOloginUsingSocialInfo:(NSMutableDictionary *)info
                    success:(void(^)())success
                    failure:(void(^)(NSError *error))failure
{
    NSAssert(info!= Nil && success!=Nil && failure!=Nil, @"One or more parameters nil");
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"nsso"] respondsToSelector:@selector(loginUsingSocialInfo:)];
    if(selectorResponse)  {
        [[sharedInstance checkIntegration:@"nsso"] performSelector: @selector(loginUsingSocialInfo:)
                                                        withObject:@{
                                                                     @"info":info,
                                                                     @"success":success,
                                                                     @"failure":failure
                                                                     }];
    } else {
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : loginUsingSocialInfo"}];
        failure(error);
    }

}
-(void)nSSOlinkSocialAccountUsingInfo:(NSMutableDictionary *)info
                              success:(void(^)())success
                              failure:(void(^)(NSError *error))failure
{
    NSAssert(info!= Nil && success!=Nil && failure!=Nil, @"One or more parameters nil");
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"nsso"] respondsToSelector:@selector(linkSocialAccountUsingInfo:)];
    if(selectorResponse)  {
        [[sharedInstance checkIntegration:@"nsso"] performSelector: @selector(linkSocialAccountUsingInfo:)
                                                        withObject:@{
                                                                     @"info":info,
                                                                     @"success":success,
                                                                     @"failure":failure
                                                                     }];
    }else {
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : linkSocialAccountUsingInfo"}];
        failure(error);
    }

}
-(void)nSSOdelinkFacebook:(void(^)())success
                  failure:(void(^)(NSError *error))failure
{
    NSAssert(success!=Nil && failure!=Nil, @"One or more parameters nil");
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"nsso"] respondsToSelector:@selector(delinkFacebook:)];
    if(selectorResponse)  {
        [[sharedInstance checkIntegration:@"nsso"] performSelector: @selector(delinkFacebook:)
                                                        withObject:@{
                                                                     @"success":success,
                                                                     @"failure":failure
                                                                     }];
    }else {
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : delinkFacebook"}];
        failure(error);
    }
}
-(void)nSSOdelinkGoogleplus:(void(^)())success
                    failure:(void(^)(NSError *error))failure;
{
    NSAssert(success!=Nil && failure!=Nil, @"One or more parameters nil");
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"nsso"] respondsToSelector:@selector(delinkGoogleplus:)];
    if(selectorResponse)  {
        [[sharedInstance checkIntegration:@"nsso"] performSelector: @selector(delinkGoogleplus:)
                                                        withObject:@{
                                                                     @"success":success,
                                                                     @"failure":failure
                                                                     }];
    }else {
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : delinkGoogleplus"}];
        failure(error);
    }
}

#pragma mark :- socialImageUpload

-(void)nSSOuploadProfilePicFromSocialUsingInfo:(NSMutableDictionary *)info
                                       success:(void(^)())success
                                       failure:(void(^)(NSError *error))failure
{
    NSAssert(info!= Nil && success!=Nil && failure!=Nil, @"One or more parameters nil");
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"nsso"] respondsToSelector:@selector(uploadProfilePicFromSocialUsingInfo:)];
    if(selectorResponse)  {
        [[sharedInstance checkIntegration:@"nsso"] performSelector: @selector(uploadProfilePicFromSocialUsingInfo:)
                                                        withObject:@{
                                                                     @"info":info,
                                                                     @"success":success,
                                                                     @"failure":failure
                                                                     }];
    } else {
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : uploadProfilePicFromSocialUsingInfo"}];
        failure(error);
    }

}

//MARK:- Upload Profile Pic
-(void)nSSOopenPhotoSelectorOnViewController:(UIViewController *)vc
                                     success:(void(^)())success
                                     failure:(void(^)(NSError *error))failure
{
    NSAssert(vc!= Nil && success!=Nil && failure!=Nil, @"One or more parameters nil");
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"nsso"] respondsToSelector:@selector(openPhotoSelectorOnViewController:)];
    if(selectorResponse){
        [[sharedInstance checkIntegration:@"nsso"] performSelector: @selector(openPhotoSelectorOnViewController:)
                                                        withObject:@{
                                                                     @"vc":vc,
                                                                     @"success":success,
                                                                     @"failure":failure
                                                                     }];
    } else {
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : openPhotoSelectorOnViewController"}];
        failure(error);
    }

}


#pragma mark - New Apis 2.2
//getUserDetails: user details will be returned in an object of type SSOUserDetails.
-(void)nSSOgetUserDetailsOnCompletion:(void(^)(NSObject * _Nullable user,NSError * _Nullable error))completion
{
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"nsso"] respondsToSelector:@selector(getUserDetailsOnCompletion:)];
    if(selectorResponse){
        [[sharedInstance checkIntegration:@"nsso"] performSelector: @selector(getUserDetailsOnCompletion:)
                                                        withObject:@{
                                                                     @"completion":ObjectOrNull(completion)
                                                                     }];
    } else {
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : getUserDetailsOnCompletion"}];
//        completion(Nil,error);
    }

}

// This Api will check if user(email/mobile) is registered with SSO). Status will be returned in an object of type SSOUserStatus.
-(void)nSSOgetStatusForIdentifier:(NSString *)identifier
                      success:(void(^)(NSObject* user))success
                      failure:(void(^)(NSError * error))failure
{
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"nsso"] respondsToSelector:@selector(getStatusForIdentifier:)];
    if(selectorResponse){
        [[sharedInstance checkIntegration:@"nsso"] performSelector: @selector(getStatusForIdentifier:)
                                                        withObject:@{
                                                                     @"identifier":ObjectOrNull(identifier),
                                                                     @"success":ObjectOrNull(success),
                                                                     @"failure":ObjectOrNull(failure)
                                                                     }];
    } else {
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : getStatusForIdentifier"}];
        failure(error);
    }


}


//update user details : first name, last name, gender(M or F), dob,city. Leave the field blank which you do not want to update.
// Updated details will be returned in an object of type SSOUserStatus
-(void)nSSOupdateUserDetails:(NSObject* )userDetails
                 success:(void(^)())success
                 failure:(void(^)(NSError* error))failure
{
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"nsso"] respondsToSelector:@selector(updateUserDetails:)];
    if(selectorResponse){
        [[sharedInstance checkIntegration:@"nsso"] performSelector: @selector(updateUserDetails:)
                                                        withObject:@{
                                                                     @"userDetails":ObjectOrNull(userDetails),
                                                                     @"success":ObjectOrNull(success),
                                                                     @"failure":ObjectOrNull(failure)
                                                                     }];
    } else {
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : updateUserDetails"}];
        failure(error);
    }
}





//Login Sessions
-(void) nSSOcreateAppSessionForTicketId:(NSString *_Nullable)ticketId completion:(void(^_Nullable)(NSDictionary* _Nullable info,NSError * _Nullable error))completion{
    
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"nsso"] respondsToSelector:@selector(createAppSessionForTicketId:)];
    if(selectorResponse){
        [[sharedInstance checkIntegration:@"nsso"] performSelector: @selector(createAppSessionForTicketId:)
                                                        withObject:@{
                                                                     @"ticketId":ObjectOrNull(ticketId),
                                                                     @"completion":ObjectOrNull(completion)
                                                                     }];
    } else {
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : createAppSessionForTicketId"}];
        completion(Nil,error);
    }
    
}

//global session:  if userDataEnabled is true, it will return session + basic info of global user else only global session will be returnd. result will be an object of type SSOSession
-(void)nSSOgetSSOGlobalSessionWithUserDataEnabled:(Boolean)userDataEnabled
                                   completion:(void(^)(NSObject * _Nullable  session, NSError * _Nullable error))completion
{
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"nsso"] respondsToSelector:@selector(getSSOGlobalSessionWithUserDataEnabled:)];
    if(selectorResponse){
        [[sharedInstance checkIntegration:@"nsso"] performSelector: @selector(getSSOGlobalSessionWithUserDataEnabled:)
                                                        withObject:@{
                                                                    @"userDataEnabled":[NSNumber numberWithBool:userDataEnabled],
                                                                     @"completion":ObjectOrNull(completion)
                                                                     }];
    } else {
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : getSSOGlobalSessionWithUserDataEnabled"}];
        completion(Nil,error);
    }

}
//app session: result will be an object of type SSOSession
-(void)nSSOgetSSOAppSessionOnCompletion:(void(^)(NSObject * session, NSError * error))completion
{
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"nsso"] respondsToSelector:@selector(getSSOAppSessionOnCompletion:)];
    if(selectorResponse){
        [[sharedInstance checkIntegration:@"nsso"] performSelector: @selector(getSSOAppSessionOnCompletion:)
                                                        withObject:@{
                                                                     @"completion":ObjectOrNull(completion)
                                                                     }];
    } else {
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : getSSOAppSessionOnCompletion"}];
        completion(Nil,error);
    }

}




// Login via Facebook,Google or Truecaller; link/d-link Facebook or Google account to SSO; upload profile pic from Gggole or Facebook account
//Note: Truecaller login is in beta phase
//For facebook and Google set oauthId and accessToken
//For Trucaller(only login supported) set payload and signature

-(void)nSSOperformSocialActivity:(NSUInteger *)options
             usingSocialInfo:(NSObject * _Nullable)info
                     success:(void(^)())success
                     failure:(void(^)(NSError *error))failure
{
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"nsso"] respondsToSelector:@selector(performSocialActivity:)];
    if(selectorResponse){
        [[sharedInstance checkIntegration:@"nsso"] performSelector: @selector(performSocialActivity:)
                                                        withObject:@{
                                                                     @"options":[NSNumber numberWithInt:options],
                                                                     @"info":ObjectOrNull(info),
                                                                     @"success":ObjectOrNull(success),
                                                                     @"failure":ObjectOrNull(failure)
                                                                     }];
    } else {
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : performSocialActivity"}];
        failure(error);
    }
}


//user signup: User can signup by providing their full details or just mobile number and name.
-(void)nSSOperformSignupActivity:(NSUInteger *)options
                     forUser:(NSObject *)user
                     success:(void(^)())success
                     failure:(void(^)(NSError *error))failure
{
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"nsso"] respondsToSelector:@selector(performSignupActivity:)];
    if(selectorResponse){
        [[sharedInstance checkIntegration:@"nsso"] performSelector: @selector(performSignupActivity:)
                                                        withObject:@{
                                                                     @"options":[NSNumber numberWithInteger:options],
                                                                     @"user":ObjectOrNull(user),
                                                                     @"success":ObjectOrNull(success),
                                                                     @"failure":ObjectOrNull(failure)
                                                                     }];
    } else {
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : performSignupActivity"}];
        failure(error);
    }
}

//profile pic upload from camera/gallery
-(void)nSSOperformPickUploadActivity:(NSUInteger *)options
                    onController:(UIViewController *)vc
                        startRequest:(void(^)())uploadStart
                         success:(void(^)())success
                         failure:(void(^)(NSError *error))failure
{
    BOOL selectorResponse = [[sharedInstance checkIntegration:@"nsso"] respondsToSelector:@selector(performPickUploadActivity:)];
    if(selectorResponse){
        [[sharedInstance checkIntegration:@"nsso"] performSelector: @selector(performPickUploadActivity:)
                                                        withObject:@{
                                                                     @"options":[NSNumber numberWithInt:options],
                                                                     @"vc":ObjectOrNull(vc),
                                                                     @"uploadStart":ObjectOrNull(uploadStart),
                                                                     @"success":ObjectOrNull(success),
                                                                     @"failure":ObjectOrNull(failure)
                                                                     }];
    } else {
        NSError *error = [[NSError alloc] initWithDomain:@"TILSDK Error" code:4200 userInfo:@{@"Error reason": @"Selector Error : performPickUploadActivity"}];
        failure(error);
    }
}


/*
 *  DMP Functions
 *
 */

-(void)dmpaddEvent:(NSString *)event forKey:(NSString *)key
{
    @try {
        [[sharedInstance checkIntegration:@"dmp"] performSelector:@selector(addEvent:) withObject:@{
                                                                       @"event":event,
                                                                        @"key":key
                                                                    }];
    } @catch (NSException *exception) {
        NSLog(@"Selector Error : %@",exception.description);
    }

}
//+(void)addEvent:(NSString *)event forKey:(NSString *)key;

-(void)dmpaddMultipleEvents:(NSString *)events forKey:(NSString *)key
{
    @try {
        [[sharedInstance checkIntegration:@"dmp"] performSelector:@selector(addMultipleEvents:) withObject:@{ @"event":events,
                            @"key":key
            }];
    } @catch (NSException *exception) {
        NSLog(@"Selector Error : %@",exception.description);
    }
}
//+(void)addMultipleEvents:(NSString *)events forKey:(NSString *)key;
/*!
 * @discussion User can use this method to manually synchronise all events to server. Elase events will be synchronise based on SDK algo
 */
-(void)dmpsynchroniseEvents
{
    @try {
        [[sharedInstance checkIntegration:@"dmp"] performSelector:@selector(synchroniseEvents)];
    } @catch (NSException *exception) {
        NSLog(@"Selector Error : %@",exception.description);
    }

}
//+(void)synchroniseEvents;


/*!
 * @discussion Use this method to set the sdk environment, weather QA OR Production.
 */
-(void)dmpsetBuildEnvironment:(NSString*)environment
{
    @try {
        [[sharedInstance checkIntegration:@"dmp"] performSelector:@selector(setBuildEnvironment:) withObject:environment];
    } @catch (NSException *exception) {
        NSLog(@"Selector Error : %@",exception.description);
    }

}
//+(void)setBuildEnvironment:(DMP_BUILD_ENVIRONMENT)environment;

/*!
 * @discussion Return array of audiences.
 */

-(void)dmpgetAudiences:(void(^)(NSArray *resultArray))completion
{
    @try {
        [[sharedInstance checkIntegration:@"dmp"] performSelector:@selector(getAudiences:) withObject:completion];
    } @catch (NSException *exception) {
        NSLog(@"Selector Error : %@",exception.description);
    }

}
//+(NSArray *)getAudiences;
@end
