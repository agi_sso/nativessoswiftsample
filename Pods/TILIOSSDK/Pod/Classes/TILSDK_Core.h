//
//  tilsdk_ios.h
//  tilsdk-ios
//
//  Created by Amit Kumar12 on 23/11/16.
//  Copyright © 2016 Amit Kumar12. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TILIntegrationFactory.h"

@protocol TILIntegrationFactory ;

@interface TILSDK_Configuration : NSObject

// Methods

+ (instancetype)configurationWithSettings:(NSDictionary *)settings;
- (void)use:(id<TILIntegrationFactory>)factory;

@end

@interface TILSDK_Core : NSObject

/*
 * Set up TIL SDK client instance.
 * @param configuration
 */

+(void)setupWithConfiguration:(TILSDK_Configuration *)configuration;

// Return sharedInstance.
+ (instancetype)sharedTILSDK;  // Class Variable
+(void)performInitializeIntegrations;   // Initialize integrations
+(void)printIntegrations;
//Return bundled integrations
+ (NSDictionary *)bundledIntegrations;
//+(void)SetShowDebugLogs: (BOOL*) showDebugLogs;
+(void) DebugLog:(NSString*)logString;

/*
 * Private Methods of class
 */

- (instancetype)initWithConfiguration:(TILSDK_Configuration *)configuration;
// Return the configuration which is used for the creation
- (TILSDK_Configuration *)configuration;

/*
 * Times Point Functions
 */
    /* To Validate the user */
-(void) tpinitWithChannelCode:(NSString *) pCode subChannelCode:(NSString*)sCode userId:(NSString *) userId deviceId:(NSString *) deviceId ticketId:(NSString*)ticketId andCompletionBlock:(void (^)(NSError* error))completionHandler;

    /* saves the user activity and send it to server */
-(void) tplogActivityWithCode:(NSString *)code transactionID:(NSString*)transactionID tranactionIdHash:(NSString*)transactionIdHash andCompletionBlock:(void (^)(NSError* error))completionHandler;

-(void) tpsetTransactionId:(NSString*)transactionId;

// On successful logout from  app removes all the user defaults of timespoint
-(void) tplogOutWithCompletionBlock:(void (^)(NSError* error))completionHandler;

    // activates timer and other functionalities
-(void)tpactivateAppWithCompletionBlock:(void (^)(NSError* error))completionHandler;

    // deactivate timer and other functionalities
-(void)tpdeactivateAppWithCompletionBlock:(void (^)(NSError* error))completionHandler;

-(void)tpinitBannerView:(UIView*)view WithType:(NSString*)bannerType andCompletionBlock:(void (^)(NSError* error))completionHandler;


/*
 * Native SSO Functions
 */

-(void)nSSOSetupForChannel:(NSString *)channel
                    siteId:(NSString *)siteid
                    teamId:(NSString *)teamId
                    completion:(void(^)(NSDictionary *info,NSError *error))completion;

#pragma mark Login via Facebook or Google
/*
 User can login so sso via Facebook or Google plus.
 App will ask user for his basic profile info and email.
 User will be loged in after success.
 */
//-(void)loginViaFacebookOnViewController:(UIViewController *)vc
//                                success:(void(^)())success
//                                failure:(void(^)(NSError *error))failure;
//-(void)loginViaGoogleOnViewController:(UIViewController *)vc
//                             delegate:(id)obj;

-(void)nSSOloginUsingSocialInfo:(NSMutableDictionary *)info
           forSocilaNetwork:(NSString *)site
                    success:(void(^)())success
                    failure:(void(^)(NSError *error))failure
__attribute__((deprecated("new available method is, nSSOperformSocialActivity:_ usingSocialInfo:_ success:_ failure:_")));

#pragma mark Login via email or mobile
/*
 User can login via their registered email or mobile.
 If user know their password they can login with that along with registered email/mobile or
 they can request a login OTP on one of their registered email/mobile.
 User will be loged inn after successfully verified their OTP or Password.
 */
//-(void)nSSOsendLoginOtpOnMobile:(NSString *)mobile
//                    success:(void(^)())success
//                    failure:(void(^)(NSError *error))failure;

-(void)nSSOsendLoginOtpOnEmail:(NSString *)email
                        mobile:(NSString *)mobile
                       success:(void(^)())success
                       failure:(void(^)(NSError *error))failure;

-(void)nSSOresendLoginOTPOnEmail:(NSString *)email
                          mobile:(NSString *)mobile
                         success:(void(^)())success
                         failure:(void(^)(NSError *error))failure;

//-(void)nSSOresendLoginOTPOnMobile:(NSString *)mobile
//                      success:(void(^)())success
//                      failure:(void(^)(NSError *error))failure;

-(void)nSSOverifyLoginOtpPassword:(NSString *)password
                            email:(NSString *)email
                           mobile:(NSString *)mobile
                          success:(void(^)())success
                          failure:(void(^)(NSError *error))failure;
//-(void)nSSOverifyLoginOtpPassword:(NSString *)password
//                     forEmail:(NSString *)email
//                      success:(void(^)())success
//                      failure:(void(^)(NSError *error))failure;


#pragma mark New user registration or signup
/*
 User can provide either mobile or email or both.
 User will provede their gender, choose a password(following standerd) and agree to send offer notifications(YES or NO).
 All fields are mandatory except email/mobile.
 A sign up otp is preferably send on mobile.
 If mobile is not given otp will be send on email.
 User can resend OTP.
 User will be loged in once OTP is successfully verified.
 */
-(void)nSSOregisterUser:(NSString *)name
             mobile:(NSString *)mobile
              email:(NSString *)email
           password:(NSString *)password
             gender:(NSString *)gender
 isSendOfferEnabled:(BOOL)isSendOfferEnabled
            success:(void(^)())success
            failure:(void(^)(NSError *error))failure
            __attribute__((deprecated("new available method is, nSSOperformSignupActivity :_ forUser:_ success:_ failure:_")));

-(void)nSSOresendSignUpOtpForEmail:(NSString *)email
                            mobile:(NSString *)mobile
                           success:(void(^)())success
                           failure:(void(^)(NSError *error))failure;

//-(void)nSSOresendSignUpOtpForMobile:(NSString *)mobile
//                        success:(void(^)())success
//                        failure:(void(^)(NSError *error))failure;

-(void)nSSOverfiySignUpOTP:(NSString *)otp
                     email:(NSString *)email
                    mobile:(NSString *)mobile
                   success:(void(^)())success
                   failure:(void(^)(NSError *error))failure;

//-(void)nSSOverfiySignUpOTP:(NSString *)otp
//             forMobile:(NSString *)mobile
//               success:(void(^)())success
//               failure:(void(^)(NSError *error))failure;



#pragma mark signOutUser
/*
 User will be loged out.
 Loging out will delete App session.
 Global session will be deleted only it is same as App session.
 */
-(void)nSSOsignOutUser:(void(^)())success
           failure:(void(^)(NSError *error))failure;

//-(BOOL)signOutUser;



#pragma mark getUserDetails
/*
 User details ccontains date of birth(dob), profile pic url(dp), user's registered email list(emailList),
 First Name, Last Name, Gender, user's registered email list(mobileList), primary email, ssoId
 and whether Facebbok or Google plus account is connected with SSO or not.
 */
-(void)nSSOgetUserDetails:(void(^)(NSDictionary *info))success
              failure:(void(^)(NSError *error))failure
              __attribute__((deprecated("new available method is, nSSOgetUserDetailsOnCompletion")));



#pragma mark change password
/*
 User can change their password.
 New password must be different than previous three passwords.
 */
-(void)nSSOchangePassword:(NSString *)oldPassword
          newPassword:(NSString *)newPassword
      confirmPassword:(NSString *)confirmPassword
              success:(void(^)())success
              failure:(void(^)(NSError *error))failure;


#pragma mark recover password
/* If user have forgetten their password.
 User can request for forgot password OTP on one of their registered email/mobile and with this OTP they can provide new password.
 New password must not match previous three passwords.
 New password will be set on successful verification.
 */
//-(void)nSSOgetForgotPasswordOTPForMobile:(NSString *)mobile
//                             success:(void(^)())success
//                             failure:(void(^)(NSError *error))failure;

-(void)nSSOgetForgotPasswordOTPForEmail:(NSString *)email
                                 mobile:(NSString *)mobile
                                success:(void(^)())success
                                failure:(void(^)(NSError *error))failure;

//-(void)nSSOresendForgotPasswordOTPForMobile:(NSString *)mobile
//                                success:(void(^)())success
//                                failure:(void(^)(NSError *error))failure;

-(void)nSSOresendForgotPasswordOTPForEmail:(NSString *)email
                                    mobile:(NSString *)mobile
                                   success:(void(^)())success
                                   failure:(void(^)(NSError *error))failure;

//-(void)nSSOverifyForgotPasswordForMobile:(NSString *)mobile
//                                 otp:(NSString *)otp
//                            password:(NSString *)password
//                     confirmPassword:(NSString *)confirmPassword
//                             success:(void(^)())success failure:(void(^)(NSError *error))failure;

-(void)nSSOverifyForgotPasswordForEmail:(NSString *)email
                                 mobile:(NSString *)mobile
                                    otp:(NSString *)otp
                               password:(NSString *)password
                        confirmPassword:(NSString *)confirmPassword
                                success:(void(^)())success
                                failure:(void(^)(NSError *error))failure;

#pragma mark Add Email or Mobile
-(void)nSSOaddAlternateEmail:(NSString *)email
                 success:(void(^)())success
                 failure:(void(^)(NSError *error))failure;
-(void)nSSOupdateMobile: (NSString *) mobile
            success:(void(^)())success
            failure:(void(^)(NSError *error))failure;
-(void)nSSOverifyAddAlternateEmailOtp:(NSString *)otp
                         forEmail:(NSString *)email
                          success:(void(^)())success
                          failure:(void(^)(NSError *error))failure;
-(void)nSSOverifyUpdateMobileOtp:(NSString *)otp
                   forMobile:(NSString *)mobile
                     success:(void(^)())success
                     failure:(void(^)(NSError *error))failure;

-(void)nSSOmigrateCurrentSessionToAppHavingTicketId:(NSString *)ticketId
                                         completion:(void(^)(NSDictionary *info,NSError *error))completion
                                         __attribute__((deprecated("new available method is,nSSOmigrateOtherSession:_ completionHandler:_")));

// Mark 2 ://
-(void)nSSOgetGlobalSessionOnCompletion:(void(^)(NSDictionary *info,NSError * error))completion
__attribute__((deprecated("new available method is,nSSOgetGlobalSessionWithUserDataEnabled:_ completion:_")));

//-(void)nSSOmigrateCurrentSessionToAppHavingTicketId:(NSString *)ticketId
//                                         completion:(void(^)(NSDictionary *info,NSError *error))completion;
-(void)nSSOgetSSOGlobalSession:(void(^)(NSDictionary *getValue))completion;
-(void)nSSOgetAppSessionOnCompletion:(void(^)(NSDictionary *info,NSError * error))completion
__attribute__((deprecated("new available method is,nSSOgetSSOAppSessionOnCompletion:_")));

-(void)nSSOcopySSOGlobalSessionToAppOnCompletion:(void(^)(NSDictionary *info,NSError * error))completion;
-(void)nSSOgetSilentLoginOptions:(void(^)(NSDictionary *loginOptions))completion;
-(void)nSSOisNewAppLogin:(void(^)(BOOL value))completion;
-(void)nSSOrenewTicket:(void(^)(void))success failure:(void(^)(NSError *error))failure;
//-(void)nSSOgetTicketId:(void(^)(NSString *ticktedId))completion; // Depricated in new version
-(void)nSSOupdateFirstName:(NSString *)firstName
                  lastName:(NSString *)lastName
                       dob:(NSString *)dob
                    gender:(NSString *)gender
                   success:(void(^)())success
                   failure:(void(^)(NSError *error))failure
                   __attribute__((deprecated("new available method is, nSSOupdateUserDetails:_ success:_ failure:_")));

-(void)nSSOdeleteAllSSOCredentialsFromApp: (void(^)())success
                                  failure:(void(^)(NSError *error))failure;
-(void)nSSOdeleteGlobalSessionIfAny:(void(^)())success
                        failure:(void(^)(NSError *error))failure;
//v2.1
//MARK:- Social link delink
-(void)nSSOloginUsingSocialInfo:(NSMutableDictionary *)info
                        success:(void(^)())success
                        failure:(void(^)(NSError *error))failure;

-(void)nSSOlinkSocialAccountUsingInfo:(NSMutableDictionary *)info
                          success:(void(^)())success
                          failure:(void(^)(NSError *error))failure
                          __attribute__((deprecated("new available method is, nSSOperformSocialActivity:_ usingSocialInfo:_ success:_ failure:_")));

-(void)nSSOdelinkFacebook:(void(^)())success
              failure:(void(^)(NSError *error))failure
              __attribute__((deprecated("new available method is, performSocialActivity:_ usingSocialInfo:_ success:_ failure:_")));

-(void)nSSOdelinkGoogleplus:(void(^)())success
                failure:(void(^)(NSError *error))failure
                __attribute__((deprecated("new available method is, performSocialActivity:_ usingSocialInfo:_ success:_ failure:_")));


#pragma mark :- socialImageUpload

-(void)nSSOuploadProfilePicFromSocialUsingInfo:(NSMutableDictionary *)info
                                   success:(void(^)())success
                                   failure:(void(^)(NSError *error))failure
                                   __attribute__((deprecated("new available method is, nSSOperformSocialActivity:_ usingSocialInfo:_ success:_ failure:_")));


//MARK:- Upload Profile Pic
-(void)nSSOopenPhotoSelectorOnViewController:(UIViewController *)vc
                                 success:(void(^)())success
                                 failure:(void(^)(NSError *error))failure
                                 __attribute__((deprecated("new available method is, nSSOperformPickUploadActivity:_ onController:_ success:_ failure:_")));

#pragma mark - New Apis 2.2
//getUserDetails: user details will be returned in an object of type SSOUserDetails.
-(void)nSSOgetUserDetailsOnCompletion:(void(^_Nullable)(NSObject* _Nullable user,NSError * _Nullable error))completion;
// Parameters Help : -(void)getUserDetailsOnCompletion:(void(^)(SSOUserDetails * _Nullable user,SSOError * _Nullable error))completion;



// This Api will check if user(email/mobile) is registered with SSO). Status will be returned in an object of type SSOUserStatus.
-(void)nSSOgetStatusForIdentifier:(NSString *_Nullable)identifier
                          success:(void(^_Nullable)(NSObject* _Nullable user))success
                          failure:(void(^_Nullable)(NSError * _Nullable error))failure;
//-(void)nSSOgetStatusForIdentifier:(NSString *)identifier
//                          success:(void(^)(SSOUserStatus* user))success
//                          failure:(void(^)(SSOError * error))failure;
// Parameters Help : -(void)getUserDetailsOnCompletion:(userDetailsBlock)completion;

//update user details : first name, last name, gender(M or F), dob,city. Leave the field blank which you do not want to update.
// Updated details will be returned in an object of type SSOUserStatus
-(void)nSSOupdateUserDetails:(NSObject* _Nullable )userDetails
                     success:(void(^_Nullable)(NSObject * _Nullable user))success
                     failure:(void(^_Nullable)(NSError* _Nullable error))failure;
//-(void)nSSOupdateUserDetails:(SSOUserUpdates* )userDetails
//                     success:(void(^)(SSOUserUpdates * _Nullable user))success
//                     failure:(void(^)(SSOError* error))failure;

//Login Sessions
//Create Session with Ticket Id
-(void) nSSOcreateAppSessionForTicketId:(NSString *_Nullable)ticketId completion:(void(^_Nullable)(NSDictionary* _Nullable info,NSError * _Nullable error))completion;
//-(void) nSSOcreateAppSessionForTicketId:(NSString *_Nullable)ticketId completion:(void(^_Nullable)(NSDictionary* _Nullable info,SSOError * _Nullable error))completion;


//global session:  if userDataEnabled is true, it will return session + basic info of global user else only global session will be returnd. result will be an object of type SSOSession
-(void)nSSOgetSSOGlobalSessionWithUserDataEnabled:(Boolean)userDataEnabled
                                       completion:(void(^_Nullable)(NSObject * _Nullable  session, NSError * _Nullable error))completion;

//-(void)nSSOgetSSOGlobalSessionWithUserDataEnabled:(Boolean)userDataEnabled
//                                       completion:(void(^_Nullable)(SSOSession * _Nullable  session, SSOError * _Nullable error))completion;

//app session: result will be an object of type SSOSession

-(void)nSSOgetSSOAppSessionOnCompletion:(void(^_Nullable)(NSObject * _Nullable session, NSError * _Nullable error))completion;
//-(void)nSSOgetSSOAppSessionOnCompletion:(void(^_Nullable)(SSOSession * _Nullable session, SSOError * _Nullable error))completion;


// Login via Facebook,Google or Truecaller; link/d-link Facebook or Google account to SSO; upload profile pic from Gggole or Facebook account
//Note: Truecaller login is in beta phase
//For facebook and Google set oauthId and accessToken
//For Trucaller(only login supported) set payload and signature

-(void)nSSOperformSocialActivity:(NSUInteger *_Nullable)options
                 usingSocialInfo:(NSObject * _Nullable)info
                         success:(void(^_Nullable)())success
                         failure:(void(^_Nullable)(NSError * _Nullable error))failure;

//-(void)nSSOperformSocialActivity:(SSOSocialActivityOptions *_Nullable)options
//                 usingSocialInfo:(SSOSocialInfo * _Nullable)info
//                         success:(void(^_Nullable)())success
//                         failure:(void(^_Nullable)(SSOError * _Nullable error))failure;

//user signup: User can signup by providing their full details or just mobile number and name.
-(void)nSSOperformSignupActivity:(NSUInteger *_Nullable)options
                         forUser:(NSObject *_Nullable)user
                         success:(void(^_Nullable)())success
                         failure:(void(^_Nullable)(NSError * _Nullable error))failure;
//-(void)nSSOperformSignupActivity:(SSOSignupOptions *_Nullable)options
//                         forUser:(SSOSignupUser *_Nullable)user
//                         success:(void(^_Nullable)())success
//                         failure:(void(^_Nullable)(SSOError * _Nullable error))failure;

//profile pic upload from camera/gallery
-(void)nSSOperformPickUploadActivity:(NSUInteger *_Nullable)options    // SSOPickUploadOptions
                        onController:(UIViewController *_Nullable)vc
                             startRequest:(void(^_Nullable)())uploadStart
                             success:(void(^_Nullable)())success
                             failure:(void(^_Nullable)(NSError * _Nullable error))failure;

//-(void)nSSOperformPickUploadActivity:(SSOPickUploadOptions *_Nullable)options    // SSOPickUploadOptions
//                        onController:(UIViewController *_Nullable)vc
//                        startRequest:(void(^_Nullable)())uploadStart
//                             success:(void(^_Nullable)())success
//                             failure:(void(^_Nullable)(SSOError * _Nullable error))failure;
//



/*
 *
 * DMP Functions Starts Here.. :)
 *
 */
-(void)dmpaddEvent:(NSString *)event forKey:(NSString *)key;
-(void)dmpaddMultipleEvents:(NSString *)events forKey:(NSString *)key;
-(void)dmpsynchroniseEvents;
-(void)dmpsetBuildEnvironment:(NSDictionary*)environment;
-(void)dmpgetAudiences:(void(^)(NSArray *resultArray))completion;
@end
