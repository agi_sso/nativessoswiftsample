//
//  SSOUserOtpPasswords.h
//  Pods
//
//  Created by Pankaj Verma on 21/04/17.
//
//

#import <Foundation/Foundation.h>

@interface SSOForgotPassword : NSObject
@property (nonatomic, nullable) NSString * email;
@property (nonatomic, nullable) NSString * mobile;
@property (nonatomic, nullable) NSString * otp;
@property (nonatomic, nullable) NSString * password;
@property (nonatomic, nullable) NSString * confirmPassword;

- (nullable instancetype)initWithDictionary:(nullable NSDictionary *) dictionary;

@end
