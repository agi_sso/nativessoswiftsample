//
//  SSOUserDetails.h
//  Pods
//
//  Created by Pankaj Verma on 05/04/17.
//
//

#import <Foundation/Foundation.h>

@interface SSOUserDetails : NSObject
@property (nonatomic, nullable, readonly) NSString *firstName;
@property (nonatomic, nullable, readonly) NSString *lastName;
@property (nonatomic, nullable, readonly) NSString * gender;
@property (nonatomic, nullable, readonly) NSString * dob;
@property (nonatomic, nullable, readonly) NSString * city;

@property (nonatomic, nullable, readonly) NSString * ssoid;
@property (nonatomic, nullable, readonly) NSString * dp;
@property (nonatomic, nullable, readonly) NSDictionary *mobileList;
@property (nonatomic, nullable, readonly) NSDictionary *emailList;
@property (nonatomic, nullable, readonly) NSString * fbConnected;
@property (nonatomic, nullable, readonly) NSString * gpConnected;
@property (nonatomic, nullable, readonly) NSString * passwordExists;
@property (nonatomic, nullable, readonly) NSString * primaryEmail;

- (nullable instancetype)initWithDictionary:(nullable NSDictionary *) dictionary;

@end
