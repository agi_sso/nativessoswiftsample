//
//  SSOHeaders.m
//  Pods
//
//  Created by Pankaj Verma on 02/05/17.
//
//

#import "SSOHeaders.h"
#define APP_BUNDEL_SITE_ID_KAY  @"siteId"
#define APP_BUNDEL_CHANNEL_KAY  @"channel"
#define APP_BUNDEL_APP_IDENTIFIER_PREFIX_KAY  @"AppIdentifierPrefix"
#define APP_BUNDEL_APP_VERSION_KAY  @"CFBundleShortVersionString"

@implementation SSOHeaders

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        _channel = [[NSBundle mainBundle] objectForInfoDictionaryKey:APP_BUNDEL_CHANNEL_KAY];
        _siteId = [[NSBundle mainBundle] objectForInfoDictionaryKey:APP_BUNDEL_SITE_ID_KAY];
        _teamId = [[NSBundle mainBundle] objectForInfoDictionaryKey:APP_BUNDEL_APP_IDENTIFIER_PREFIX_KAY];
        _appVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:APP_BUNDEL_APP_VERSION_KAY];
        _platform = @"ios";
        _getData = @"false";
        
    }
    return self;
}

- (nullable instancetype)initWithDictionary:(nullable NSDictionary *) dictionary
{
    if (self = [super init])
    {
        if (self != nil)
        {
            if (![dictionary isKindOfClass:[NSNull class]])
            {
                [self setValuesForKeysWithDictionary:dictionary];
            }
            
        }
    }
    return self;
}

-(void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    NSLog(@"undefined key  = %@",key);
}
-(void) setGetData:(NSString * _Nullable)getData
{
    _getData = getData;
}
-(void)setChannel:(NSString * _Nullable)channel
{
    _channel = channel;
}

-(void)setSiteId:(NSString * _Nullable)siteId
{
    _siteId = siteId;
}

-(void)setTeamId:(NSString * _Nullable)teamId
{
    _teamId = teamId;
}

@end
