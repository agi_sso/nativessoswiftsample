//
//  SSOSocialInfo.h
//  Pods
//
//  Created by Pankaj Verma on 20/04/17.
//
//

#import <Foundation/Foundation.h>
#define SSOSOCIAL_OAUTH_ID_KEY @"oauthId"
#define SSOSOCIAL_ACCESS_TOKEN_KEY @"accessToken"
#define SSOSOCIAL_PAYLOAD_KEY @"payload"
#define SSOSOCIAL_SIGNATURE_KEY @"signature"

#define SSOSOCIAL_OAUTH_SITEID_KEY @"oauthsiteid"
#define SSOSOCIAL_OAUTH_SITEID_NEW_KEY @"oauthSiteId"

//login Types
#define SSOSOCIAL_TYPE_GOOGLEPLUS  @"googleplus"
#define SSOSOCIAL_TYPE_FACEBOOK  @"facebook"
#define SSOSOCIAL_TYPE_SSO  @"sso"
#define SSOSOCIAL_TYPE_TRUECALLER  @"truecaller"


@interface SSOSocialInfo : NSObject

//Facebook and Google
@property (nonatomic, nullable) NSString *oauthId;
@property (nonatomic, nullable) NSString *accessToken;
//Truecaller
@property (nonatomic, nullable) NSString *payload;
@property (nonatomic, nullable) NSString *signature;

- (nullable instancetype)initWithDictionary:(nullable NSDictionary *) dictionary;

@end
