
//
//  NSSOCrossAppLoginManager.m
//  Pods
//
//  Created by Pankaj Verma on 10/13/16.
//
//

#import "NSSOCrossAppLoginManager.h"
#import "NSSODownloadManager.h"
#import "NSSOGlobal.h"
#import "SSOIdentifiers.h"
#import "SSOForgotPassword.h"
#import <objc/runtime.h>

#define CASE(str)                       if ([__s__ isEqualToString:(str)])
#define SWITCH(s)                       for (NSString *__s__ = (s); ; )
#define DEFAULT

typedef NS_OPTIONS(NSUInteger, SSOSaveSessionOptions)
{
    SSOMigratedAppOnlySession = (1UL << 0),
    SSOCopiedSession = (1UL << 1),
    SSODefaultSession = (1UL << 2)
} NS_ENUM_AVAILABLE(10_7, 5_0);


NSString * const USER_OLD_PASSWORD_KEY = @"oldPassword";
NSString * const USER_NEW_PASSWORD_KEY = @"newPassword";
NSString * const USER_CONFIRM_PASSWORD_KEY = @"confirmPassword";

//SSO Base Urls
NSString * const SSOBaseUrl = @"jsso.indiatimes.com";
NSString * const SSOmSocialBaseUrl = @"socialappsintegrator.indiatimes.com";

//Relative Urls
NSString * const getLoginOtpPath = @"getLoginOtp";
NSString * const verifyLoginOtpPasswordPath = @"verifyLoginOtpPassword";
NSString * const resendForgotPasswordOTPPath = @"resendForgotPasswordOTP";
NSString * const getUserDetailsPath = @"getUserDetails";
NSString * const signOutUserPath = @"signOutUser";
NSString * const changePasswordPath = @"changePassword";
NSString * const getForgotPasswordOtpPath = @"getForgotPasswordOtp";
NSString * const verifyForgotPasswordPath = @"verifyForgotPassword";
NSString * const registerUserAPIPath = @"registerUser";
NSString * const verifySignUpOTPPath = @"verifySignUpOTP";
NSString * const resendSignUpOtp = @"resendSignUpOtp";
NSString * const registerOnlyMobilePath = @"registerOnlyMobile";
NSString * const getNewTicket = @"getNewTicket";
NSString * const renewTicketPath = @"renewTicket";
NSString * const addAlternateEmailPath = @"addAlternateEmail";
NSString * const verifyAlternateEmailPath = @"verifyAlternateEmail";
NSString * const updateMobilePath = @"updateMobile";
NSString * const verifyMobilePath = @"verifyMobile";
NSString * const updateUserDetailsPath = @"updateUserDetails";
NSString * const linkSocialPath = @"linkSocial";
NSString * const delinkSocialPath = @"delinkSocial";
NSString * const uploadProfilePicPath = @"uploadProfilePic";
NSString * const socialImageUploadPath = @"socialImageUpload";
NSString * const checkUserExistsPath = @"checkUserExists";
NSString * const truecallerLoginPath = @"truecaller";

//Relative Urls mSocial
//NSString * const trapPageUrlPath = @"/msocialsite/app/trappagerequest";
NSString * const signInWithGoogleResponse = @"/msocialsite/app/googleplusresponse";
NSString * const signInWithFacebookResponse = @"/msocialsite/app/facebookresponse";

NSString * const SSO_SSEC_REQ_KEY = @"ssecreq";
NSString * const SSO_SITE_REG_KEY = @"sitereg";
NSString * const SSO_DEVICEiD_KEY = @"deviceid";
NSString * const SSO_DEVICEID_KEY = @"deviceId";

@interface NSSOCrossAppLoginManager ()<UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIActionSheetDelegate>

@end

@implementation NSSOCrossAppLoginManager

NSString *ssoid = @"";

static NSSOCrossAppLoginManager *singletonObject = nil;

+ (id) sharedLoginManager
{
    static dispatch_once_t pred;
    if (! singletonObject)
    {
        dispatch_once(&pred, ^{
            singletonObject = [[NSSOCrossAppLoginManager alloc] init];
        });
    }
    return singletonObject;
}

- (id)init
{
    static dispatch_once_t pred;
    
    if (! singletonObject)
    {
        dispatch_once(&pred, ^{
            singletonObject = [super init];
        });
    }
    return singletonObject;
}

-(void)ssoSetupForChannel:(NSString *)channel
                   siteId:(NSString *)siteId
                   teamId:(NSString *)teamId
               completion:(completionBlock)completion
{
    appHeader = [[SSOHeaders alloc] init];
    appHeader.channel = channel;
    appHeader.siteId = siteId;
    appHeader.teamId = [NSString stringWithFormat:@"%@.",teamId];

    appSession = [[SSOSession alloc] initWithAppKeychain];
    globalSession = [[SSOSession alloc] initWithSharedKeychain];
    
    if ([appSession.tgId length]!=0)
    {
        if ([globalSession.tgId length]==0)
        {
            [globalSession initializeGlobalSessionWithTgId:appSession.tgId];
        }
        completion(NULL,NULL);
        return;
    }
    if ([globalSession.tgId length]!=0)
    {
        [appSession initializeAppSessionWithTgId:globalSession.tgId];
        completion(NULL,NULL);
        return;
    }
    
    [self gettgidOnCompletion:^(NSDictionary *info, SSOError *error)
     {
         completion(info,error);
     }];
}

#pragma mark Social login
-(void)loginUsingSocialInfo:(NSDictionary *)info
                    success:(voidBlock)success
                    failure:(errorBlock)failure
{
    NSString * baseUrl = SSOmSocialBaseUrl;
    NSString *socialLoginPath = signInWithFacebookResponse;
    NSUInteger scope = ssomSocial;
    NSDictionary *params = @{};
    NSDictionary *queries = @{};
    NSString *site = [info valueForKey:SSOSOCIAL_OAUTH_SITEID_KEY];
    
    SWITCH(site)
    {
        CASE(SSOSOCIAL_TYPE_FACEBOOK)
        {
            socialLoginPath = signInWithFacebookResponse;
            queries = [self paramsForSocialInfo:info];
            baseUrl = SSOmSocialBaseUrl;
            scope = ssomSocial;
            break;
        }
        CASE(SSOSOCIAL_TYPE_GOOGLEPLUS)
        {
            socialLoginPath = signInWithGoogleResponse;
            queries = [self paramsForSocialInfo:info];
            baseUrl = SSOmSocialBaseUrl;
            scope = ssomSocial;
            break;
        }
        CASE(SSOSOCIAL_TYPE_TRUECALLER)
        {
            socialLoginPath = truecallerLoginPath;
            params = info;
            baseUrl = SSOBaseUrl;
            scope = ssoBase;
            break;
        }
        DEFAULT
        {
            break;
        }
    }
    
    //    [self validateTruecallerParams:params
    //                     andFbGpParams:queries
    //                           success:^
    //     {
    [[NSSODownloadManager ssoSharedManager] downloadDataForBaseUrl:baseUrl
                                                             scope:scope
                                                              path:socialLoginPath
                                                            params:params
                                                           queries:queries
                                                 completionHandler:^(NSDictionary * _Nullable dataDictionary, SSOError * _Nullable error)
     {
         if (error != NULL)
         {
             failure(error);
             return ;
         }
         
         SSOSession *k_session = [[SSOSession alloc] initWithDictionary:dataDictionary];
         [k_session setType:site];
         [self saveSession:k_session option:SSODefaultSession];
         success();
         
     }];
    
    //     }
    //                           failure:^(SSOError * _Nullable error)
    //     {
    //         failure(error);
    //     }];
}

//MARK:- Login via email or mobile
-(void)sendLoginOtpOnEmail:(NSString *)email
                    mobile:(NSString *)mobile
                   success:(voidBlock)success
                   failure:(errorBlock)failure
{
    
    SSOIdentifiers *op = [[SSOIdentifiers alloc] init];
    op.mobile = mobile;
    op.email = email;
    NSDictionary *info = [self dictionaryWithPropertiesOfObject:op];
    
    [[NSSODownloadManager ssoSharedManager] downloadDataForBaseUrl:SSOBaseUrl
                                                             scope:ssoBase
                                                              path:getLoginOtpPath
                                                            params:info
                                                           queries:NULL
                                                 completionHandler:^(NSDictionary * _Nullable dataDictionary, SSOError * _Nullable error)
     {
         if (error != NULL)
         {
             failure(error);
             return ;
         }
         success();
     }];
}

-(void)resendLoginOTPOnEmail:(NSString *)email
                      mobile:(NSString *)mobile
                     success:(voidBlock)success
                     failure:(errorBlock)failure
{
    
    SSOIdentifiers *op = [[SSOIdentifiers alloc] init];
    op.mobile = mobile;
    op.email = email;
    NSDictionary *userInfo = [self dictionaryWithPropertiesOfObject:op];
    
    [[NSSODownloadManager ssoSharedManager] downloadDataForBaseUrl:SSOBaseUrl
                                                             scope:ssoBase
                                                              path:getLoginOtpPath
                                                            params:userInfo
                                                           queries:NULL
                                                 completionHandler:^(NSDictionary * _Nullable dataDictionary, SSOError * _Nullable error)
     {
         if (error != NULL)
         {
             failure(error);
             return ;
         }
         success();
     }];
}

-(void)verifyLoginOtpPassword:(NSString *)password
                        email:(NSString *)email
                       mobile:(NSString *)mobile
                      success:(voidBlock)success
                      failure:(errorBlock)failure
{
    SSOIdentifiers *op = [[SSOIdentifiers alloc] init];
    op.mobile = mobile;
    op.email = email;
    op.password = password;
    NSDictionary *userInfo = [self dictionaryWithPropertiesOfObject:op];
    
    
    [[NSSODownloadManager ssoSharedManager] downloadDataForBaseUrl:SSOBaseUrl
                                                             scope:ssoBase
                                                              path:verifyLoginOtpPasswordPath
                                                            params:userInfo
                                                           queries:NULL
                                                 completionHandler:^(NSDictionary * _Nullable dataDictionary, SSOError * _Nullable error)
     {
         
         if (error != NULL)
         {
             failure(error);
             return ;
         }
         
         SSOSession *k_session = [[SSOSession alloc] initWithDictionary:dataDictionary];
         k_session.type = SSOSOCIAL_TYPE_SSO;
         [self saveSession:k_session option:SSODefaultSession];
         
         success();
     }];
}

//MARK:- New user registration / signup
-(void)registerUser:(NSString *)name
             mobile:(NSString *)mobile
              email:(NSString *)email
           password:(NSString *)password
             gender:(NSString *)gender
 isSendOfferEnabled:(BOOL)isSendOfferEnabled
            success:(voidBlock)success
            failure:(errorBlock)failure
{
    SSOSignupUser * user = [[SSOSignupUser alloc] init];
    user.name = name;
    user.mobile = mobile;
    user.email = email;
    user.password = password;
    user.gender = gender;
    // user.isSendOfferEnabled = isSendOfferEnabled; //check
    
    NSDictionary *userInfo = [self dictionaryWithPropertiesOfObject:user];
    
    
    [[NSSODownloadManager ssoSharedManager] downloadDataForBaseUrl:SSOBaseUrl
                                                             scope:ssoBase
                                                              path:registerUserAPIPath
                                                            params:userInfo
                                                           queries:NULL
                                                 completionHandler:^(NSDictionary * _Nullable dataDictionary, SSOError * _Nullable error)
     {
         
         if (error != NULL)
         {
             failure(error);
             return ;
         }
         
         SSOSignupUser *u = [[SSOSignupUser alloc] initWithDictionary:dataDictionary];
         ssoid = u.ssoid;
         // Signup otp sent
         success();
     }];
    
    
}

//Email
-(void)verfiySignUpOTP:(NSString *)otp
                 email:(NSString *)email
                mobile:(NSString *)mobile
               success:(voidBlock)success
               failure:(errorBlock)failure
{
    SSOSignupUser * user = [[SSOSignupUser alloc] init];
    user.mobile = mobile;
    user.email = email;
    user.otp = otp;
    user.ssoid  = ssoid;
    
    NSDictionary *userInfo = [self dictionaryWithPropertiesOfObject:user];
    
    
    
    [[NSSODownloadManager ssoSharedManager] downloadDataForBaseUrl:SSOBaseUrl
                                                             scope:ssoBase
                                                              path:verifySignUpOTPPath
                                                            params:userInfo
                                                           queries:NULL
                                                 completionHandler:^(NSDictionary * _Nullable dataDictionary, SSOError * _Nullable error)
     {
         
         if (error != NULL)
         {
             failure(error);
             return ;
         }
         
         SSOSession *k_session = [[SSOSession alloc] initWithDictionary:dataDictionary];
         k_session.type = SSOSOCIAL_TYPE_SSO;
         [self saveSession:k_session option:SSODefaultSession];
         // Sussessfully signed up user is now loged in
         success();
     }];
}

-(void)resendSignUpOtpForEmail:(NSString *)email
                        mobile:(NSString *)mobile
                       success:(voidBlock)success
                       failure:(errorBlock)failure
{
    SSOSignupUser * user = [[SSOSignupUser alloc] init];
    user.mobile = mobile;
    user.email = email;
    user.ssoid  = ssoid;
    
    NSDictionary *userInfo = [self dictionaryWithPropertiesOfObject:user];
    
    [[NSSODownloadManager ssoSharedManager] downloadDataForBaseUrl:SSOBaseUrl
                                                             scope:ssoBase
                                                              path:resendSignUpOtp
                                                            params:userInfo
                                                           queries:NULL
                                                 completionHandler:^(NSDictionary * _Nullable dataDictionary, SSOError * _Nullable error)
     {
         
         if (error != NULL)
         {
             failure(error);
             return ;
         }
         success();
     }];
}

-(void)registerOnlyMobile:(NSString *)mobile
                     name:(NSString *)name
                   gender:(NSString *)gender
                  success:(voidBlock)success
                  failure:(errorBlock)failure
{
    
    SSOSignupUser * user = [[SSOSignupUser alloc] init];
    user.name = name;
    user.mobile = mobile;
    user.gender = gender;
    
    NSDictionary *userInfo = [self dictionaryWithPropertiesOfObject:user];
    [[NSSODownloadManager ssoSharedManager] downloadDataForBaseUrl:SSOBaseUrl
                                                             scope:ssoBase
                                                              path:registerOnlyMobilePath
                                                            params:userInfo
                                                           queries:NULL
                                                 completionHandler:^(NSDictionary * _Nullable dataDictionary, SSOError * _Nullable error)
     {
         
         if (error != NULL)
         {
             failure(error);
             return ;
         }
         SSOSignupUser *u = [[SSOSignupUser alloc] initWithDictionary:dataDictionary];
         ssoid = u.ssoid;
         // Signup otp sent
         success();
     }];
}

//MARK:- signOutUser
-(void)signOutUser: (voidBlock)success
           failure:(errorBlock)failure
{
    if (![self isSDKInitialized])
    {
        failure([SSOError errorWithCode:SSOSDKErrorCodeSDKNotInitialized]);
        return;
    }
    
    [[NSSODownloadManager ssoSharedManager] downloadDataForBaseUrl:SSOBaseUrl
                                                             scope:ssoBase
                                                              path:signOutUserPath
                                                            params:NULL
                                                           queries:NULL
                                                 completionHandler:^(NSDictionary * _Nullable dataDictionary, SSOError * _Nullable error)
     {
         [self removeSessions];
         
         if (error != NULL)
         {
             failure(error);
             return ;
         }
         
         success();
     }];
}


//MARK:- getUserDetails
-(void)getUserDetails:(void(^)(NSDictionary *info))success
              failure:(void(^)(NSError * _Nullable error))failure
{
[self getUserDetailsOnCompletion:^(SSOUserDetails * _Nullable user, SSOError * _Nullable error) {
    if (error)
    {
        failure(error);
        return ;
    }
    success([self dictionaryWithPropertiesOfObject:user]);
}];
}
-(void)getUserDetailsOnCompletion:(userDetailsBlock)completion
{
    [[NSSODownloadManager ssoSharedManager] downloadDataForBaseUrl:SSOBaseUrl
                                                             scope:ssoBase
                                                              path:getUserDetailsPath
                                                            params:NULL                                                           queries:NULL
                                                 completionHandler:^(NSDictionary * _Nullable dataDictionary, SSOError * _Nullable error)
     {
         if (error != NULL)
         {
             [self removeSessionForUnauthorizedAccessCode:error.code];
             completion(NULL,error);
             return ;
         }
         SSOUserDetails * u = [[SSOUserDetails alloc] initWithDictionary:dataDictionary];
         completion(u,NULL);
         
         if ([globalSession.ssec isEqualToString: appSession.ssec]) //check for nil session
         {
             [[NSSODownloadManager ssoSharedManager] downloadDataForBaseUrl:SSOBaseUrl
                                                                      scope:ssoGlobal
                                                                       path:renewTicketPath
                                                                     params:NULL                                                           queries:NULL
                                                          completionHandler:^(NSDictionary * _Nullable dataDictionary, SSOError * _Nullable error)
              {
              }];
         }
     }];
}


//MARK:- changePassword
-(void)changePassword:(NSString *)oldPassword
          newPassword:(NSString *)newPassword
      confirmPassword:(NSString *)confirmPassword
              success:(voidBlock)success
              failure:(errorBlock)failure
{
    
    NSMutableDictionary * userInfo = [[NSMutableDictionary alloc] init];
    [userInfo setValue:oldPassword forKey:USER_OLD_PASSWORD_KEY];
    [userInfo setValue:newPassword forKey:USER_NEW_PASSWORD_KEY];
    [userInfo setValue:confirmPassword forKey:USER_CONFIRM_PASSWORD_KEY];
    
    [[NSSODownloadManager ssoSharedManager] downloadDataForBaseUrl:SSOBaseUrl
                                                             scope:ssoBase
                                                              path:changePasswordPath
                                                            params:userInfo
                                                           queries:NULL
                                                 completionHandler:^(NSDictionary * _Nullable dataDictionary, SSOError * _Nullable error)
     {
         
         if (error != NULL)
         {
             [self removeSessionForUnauthorizedAccessCode:error.code];
             failure(error);
             return ;
         }
         success();
     }];
}

#pragma mark Chek if User's email or mobile is registered
-(void)getStatusForIdentifier:(NSString *)identifier
                      success:(userStatusBlock)success
                      failure:(errorBlock)failure
{
    SSOIdentifiers *op = [[SSOIdentifiers alloc] init];
    op.identifier = identifier;
    NSDictionary *userInfo = [self dictionaryWithPropertiesOfObject:op];
    
    
    [[NSSODownloadManager ssoSharedManager] downloadDataForBaseUrl:SSOBaseUrl
                                                             scope:ssoBase
                                                              path:checkUserExistsPath
                                                            params:userInfo
                                                           queries:NULL
                                                 completionHandler:^(NSDictionary * _Nullable dataDictionary, SSOError * _Nullable error)
     {
         
         if (error != NULL)
         {
             failure(error);
             return ;
         }
         SSOUserStatus *u = [[SSOUserStatus alloc] initWithDictionary:dataDictionary];
         success(u);
     }];
    
}

#pragma mark recover password
-(void)getForgotPasswordOTPForEmail:(NSString *)email
                             mobile:(NSString *)mobile
                            success:(voidBlock)success
                            failure:(errorBlock)failure
{
    SSOForgotPassword *fp = [[SSOForgotPassword alloc] init];
    
    fp.mobile = mobile;
    fp.email = email;
    NSDictionary * userInfo = [self dictionaryWithPropertiesOfObject: fp];
    
    
    [[NSSODownloadManager ssoSharedManager] downloadDataForBaseUrl:SSOBaseUrl
                                                             scope:ssoBase
                                                              path:getForgotPasswordOtpPath
                                                            params:userInfo
                                                           queries:NULL
                                                 completionHandler:^(NSDictionary * _Nullable dataDictionary, SSOError * _Nullable error)
     {
         if (error != NULL)
         {
             failure(error);
             return ;
         }
         success();
     }];
}

//Email
-(void)resendForgotPasswordOTPForEmail:(NSString *)email
                                mobile:(NSString *)mobile
                               success:(voidBlock)success
                               failure:(errorBlock)failure
{
    
    SSOForgotPassword *op = [[SSOForgotPassword alloc] init];
    op.mobile = mobile;
    op.email = email;
    NSDictionary * userInfo = [self dictionaryWithPropertiesOfObject: op];
    
    [[NSSODownloadManager ssoSharedManager] downloadDataForBaseUrl:SSOBaseUrl
                                                             scope:ssoBase
                                                              path:resendForgotPasswordOTPPath
                                                            params:userInfo
                                                           queries:NULL
                                                 completionHandler:^(NSDictionary * _Nullable dataDictionary, SSOError * _Nullable error)
     {
         if (error != NULL)
         {
             failure(error);
             return ;
         }
         success();
     }];
}

//Email
-(void)verifyForgotPasswordForEmail:(NSString *)email
                             mobile:(NSString *)mobile
                                otp:(NSString *)otp
                           password:(NSString *)password
                    confirmPassword:(NSString *)confirmPassword
                            success:(voidBlock)success
                            failure:(errorBlock)failure
{
    
    
    SSOForgotPassword *op = [[SSOForgotPassword alloc] init];
    
    op.mobile = mobile;
    op.email = email;
    op.otp = otp;
    op.password = password;
    op.confirmPassword = confirmPassword;
    NSDictionary * userInfo = [self dictionaryWithPropertiesOfObject: op];
    
    [[NSSODownloadManager ssoSharedManager] downloadDataForBaseUrl:SSOBaseUrl
                                                             scope:ssoBase
                                                              path:verifyForgotPasswordPath
                                                            params:userInfo
                                                           queries:NULL
                                                 completionHandler:^(NSDictionary * _Nullable dataDictionary, SSOError * _Nullable error)
     {
         if (error != NULL)
         {
             failure(error);
             return ;
         }
         success();
     }];
}

#pragma mark Add Email or Mobile
-(void)addAlternateEmail:(NSString *)email
                 success:(voidBlock)success
                 failure:(errorBlock)failure

{
    
    
    SSOIdentifiers * i = [[SSOIdentifiers alloc] init];
    i.email = email;
    NSDictionary * userInfo = [self dictionaryWithPropertiesOfObject: i];
    [[NSSODownloadManager ssoSharedManager] downloadDataForBaseUrl:SSOBaseUrl
                                                             scope:ssoBase
                                                              path:addAlternateEmailPath
                                                            params:userInfo
                                                           queries:NULL
                                                 completionHandler:^(NSDictionary * _Nullable dataDictionary, SSOError * _Nullable error)
     {
         if (error != NULL)
         {
             [self removeSessionForUnauthorizedAccessCode:error.code];
             failure(error);
             return ;
         }
         // Password changed
         success();
     }];
}

-(void)updateMobile: (NSString *) mobile
            success:(voidBlock)success
            failure:(errorBlock)failure
{
    
    SSOIdentifiers * i = [[SSOIdentifiers alloc] init];
    i.mobile = mobile;
    NSDictionary * userInfo = [self dictionaryWithPropertiesOfObject: i];
    
    
    [[NSSODownloadManager ssoSharedManager] downloadDataForBaseUrl:SSOBaseUrl
                                                             scope:ssoBase
                                                              path: updateMobilePath
                                                            params:userInfo
                                                           queries:NULL
                                                 completionHandler:^(NSDictionary * _Nullable dataDictionary, SSOError * _Nullable error)
     {
         
         if (error != NULL)
         {
             [self removeSessionForUnauthorizedAccessCode:error.code];
             failure(error);
             return ;
         }
         success();
     }];
}

-(void)verifyAddAlternateEmailOtp:(NSString *)otp
                         forEmail:(NSString *)email
                          success:(voidBlock)success
                          failure:(errorBlock)failure
{
    
    SSOIdentifiers * i = [[SSOIdentifiers alloc] init];
    i.email = email;
    i.otp = otp;
    NSDictionary * userInfo = [self dictionaryWithPropertiesOfObject: i];
    
    [[NSSODownloadManager ssoSharedManager] downloadDataForBaseUrl:SSOBaseUrl
                                                             scope:ssoBase
                                                              path:verifyAlternateEmailPath
                                                            params:userInfo
                                                           queries:NULL
                                                 completionHandler:^(NSDictionary * _Nullable dataDictionary, SSOError * _Nullable error)
     {
         if (error != NULL)
         {
             [self removeSessionForUnauthorizedAccessCode:error.code];
             failure(error);
             return ;
         }
         // Password changed
         success();
     }];
}

-(void)verifyUpdateMobileOtp:(NSString *)otp
                   forMobile:(NSString *)mobile
                     success:(voidBlock)success
                     failure:(errorBlock)failure
{
    SSOIdentifiers * i = [[SSOIdentifiers alloc] init];
    i.mobile = mobile;
    i.otp = otp;
    NSDictionary * userInfo = [self dictionaryWithPropertiesOfObject: i];
    
    
    
    [[NSSODownloadManager ssoSharedManager] downloadDataForBaseUrl:SSOBaseUrl
                                                             scope:ssoBase
                                                              path:verifyMobilePath
                                                            params:userInfo
                                                           queries:NULL
                                                 completionHandler:^(NSDictionary * _Nullable dataDictionary, SSOError * _Nullable error)
     {
         
         if (error != NULL)
         {
             [self removeSessionForUnauthorizedAccessCode:error.code];
             failure(error);
             return ;
         }
         // Password changed
         success();
     }];
}

//MARK:- version 2.0
//MARK:- Login Sessions
-(void)migrateCurrentSessionToAppHavingTicketId:(NSString *)ticketId
                                     completion:(completionBlock)completion
{
    if (![self isSDKInitialized])
    {
        completion(NULL,[SSOError errorWithCode:SSOSDKErrorCodeSDKNotInitialized]);
        return;
    }
    if ([ticketId isKindOfClass:[NSString class]])
    {
        MIGRATE_TICKETID = ticketId;
    }
    
    
    
    [[NSSODownloadManager ssoSharedManager] downloadDataForBaseUrl:SSOBaseUrl
                                                             scope:ssoBase
                                                              path:getSsecFromTicket
                                                            params:NULL
                                                           queries:NULL
                                                 completionHandler:^(NSDictionary * _Nullable dataDictionary, SSOError * _Nullable error)
     {
         if (error != NULL)
         {
             completion(NULL,error);
             return ;
         }
         
         SSOSession *k_session = [[SSOSession alloc] initWithDictionary:dataDictionary];
         
         //         if ([k_session.unverifiedUser isEqualToString:@"0"] && [globalSession.ssec length] == 0)
         //         {
         //             [self saveSession:k_session option:SSODefaultSession];
         //         }
         //         else
         //             [self saveSession:k_session option:SSOMigratedAppOnlySession];
         
         if ([k_session.unverifiedUser  isEqualToString:@"1"])
         {
             completion(NULL,[SSOError errorWithCode:SSOSDKErrorCodeMigrateUnverifiedUserError]);
             return;
         }
         if ([globalSession.ssec length] == 0)
         {
             [self saveSession:k_session option:SSODefaultSession];
         }
         else
         {
             // else copy to local only
             [self saveSession:k_session option:SSOMigratedAppOnlySession];
         }
         completion(NULL,NULL);
         
     }];
}

-(void)getGlobalSessionOnCompletion:(completionBlock)completion
{
    [self getSSOGlobalSessionWithUserDataEnabled:false
                                   completion:^(SSOSession * _Nullable session, SSOError * _Nullable error)
     {
         completion([self dictionaryWithPropertiesOfObject:session],error);
     }];
}
-(void)getSSOGlobalSessionWithUserDataEnabled:(Boolean)userDataEnabled
                                completion:(sessionBlock)completion
{
    if (![self isSDKInitialized])
    {
        completion(NULL,[SSOError errorWithCode:SSOSDKErrorCodeSDKNotInitialized]);
        return;
    }
    
    [globalSession updateGlobalSession];
    if ([globalSession.ssec length] == 0)
    {
        completion(NULL,[SSOError errorWithCode:SSOSDKErrorCodeSessionNotFound]);
        return;
    }
    
    if(userDataEnabled)
    {
        [appHeader setGetData:@"true"];
    }
    
    [[NSSODownloadManager ssoSharedManager] downloadDataForBaseUrl:SSOBaseUrl
                                                             scope:ssoGlobal
                                                              path:renewTicketPath
                                                            params:NULL
                                                           queries:NULL
                                                 completionHandler:^(NSDictionary * _Nullable dataDictionary, SSOError * _Nullable error)
     {
         [appHeader setGetData:@"false"];
         if (error != NULL)
         {
             if (error.code == SSOSDKErrorCodeUnauthorizedAccess)
             {
                 [self removeGlobalSession];
             }
             completion(NULL,error);
             return ;
         }
         
         
         SSOBasicUserDetails * u = [[SSOBasicUserDetails alloc] initWithDictionary:dataDictionary];
         globalSession.user = u;
         completion(globalSession,NULL);
     }];
}


-(void)getAppSessionOnCompletion:(completionBlock)completion
{
[self getSSOAppSessionOnCompletion:^(SSOSession * _Nullable session, SSOError * _Nullable error) {
    completion([self dictionaryWithPropertiesOfObject:session],error);
}];
}

-(void)getSSOAppSessionOnCompletion:(sessionBlock)completion

{
    if (![self isSDKInitialized])
    {
        completion(NULL,[SSOError errorWithCode:SSOSDKErrorCodeSDKNotInitialized]);
        return;
    }
    [appSession updateAppSession];
    completion(appSession,NULL);
}


-(void)copySSOGlobalSessionToAppOnCompletion:(completionBlock)completion
{
    if (![self isSDKInitialized])
    {
        completion(NULL,[SSOError errorWithCode:SSOSDKErrorCodeSDKNotInitialized]);
        return;
    }
    [globalSession updateGlobalSession];
    if ([globalSession.ssec length]==0)
    {
        completion(NULL,[SSOError errorWithCode:SSOSDKErrorCodeSessionNotFound]);
        return ;
    }
    
    [appSession initializeAppSessionWithSession:globalSession];
    [self getNewTicketOnCompletion:^(SSOSession * _Nullable session, SSOError * _Nullable error)
     {
         if (error == NULL)
         {
             
             NSString *newTicket = session.ticketId;
             [appSession setAppSessionNewTicketId:newTicket];
              completion(NULL,NULL);
         }
         else
         {
             [self removeSessionForUnauthorizedAccessCode:error.code];
             completion(NULL,error);
             return ;
         }
     }];
}


//MARK:- Renew ticket
-(void)renewTicket:(voidBlock)success
           failure:(errorBlock)failure
{
    [[NSSODownloadManager ssoSharedManager] downloadDataForBaseUrl:SSOBaseUrl
                                                             scope:ssoBase
                                                              path:renewTicketPath
                                                            params:NULL
                                                           queries:NULL
                                                 completionHandler:^(NSDictionary * _Nullable dataDictionary, SSOError * _Nullable error)
     {
         if (error != NULL)
         {
             [self removeSessionForUnauthorizedAccessCode:error.code];
             failure(error);
             return ;
         }
         success();
     }];
}


//MARK:- Update user details
-(void)updateFirstName:(NSString *)firstName
              lastName:(NSString *)lastName
                   dob:(NSString *)dob
                gender:(NSString *)gender
               success:(voidBlock)success
               failure:(errorBlock)failure
{
    SSOUserUpdates *updateDetails = [[SSOUserUpdates alloc] init];
    updateDetails.firstName = firstName;
    updateDetails.lastName = lastName;
    updateDetails.dob = dob;
    updateDetails.gender = gender;
    NSDictionary *info = [self dictionaryWithPropertiesOfObject:updateDetails];
    [[NSSODownloadManager ssoSharedManager] downloadDataForBaseUrl:SSOBaseUrl
                                                             scope:ssoBase
                                                              path:updateUserDetailsPath
                                                            params:info
                                                           queries:NULL
                                                 completionHandler:^(NSDictionary * _Nullable dataDictionary, SSOError * _Nullable error)
     {
         if (error != NULL)
         {
             [self removeSessionForUnauthorizedAccessCode:error.code];
             failure(error);
             return ;
         }
         //SSOUserUpdates *u = [[SSOUserUpdates alloc] initWithDictionary:dataDictionary];
         //success(u);
         success();
     }];
}


//MARK:- Social link delink
-(void)linkSocialAccountUsingInfo:(NSDictionary *)info
                          success:(voidBlock)success
                          failure:(errorBlock)failure
{
    NSMutableDictionary *ssoInfo = [info mutableCopy];
    
    [ssoInfo  setValue:info[SSOSOCIAL_OAUTH_SITEID_KEY] forKey:SSOSOCIAL_OAUTH_SITEID_NEW_KEY];
    
    //    [self validateTruecallerParams:NULL
    //                     andFbGpParams:ssoInfo
    //                           success:^{
    [[NSSODownloadManager ssoSharedManager] downloadDataForBaseUrl:SSOBaseUrl
                                                             scope:ssoBase
                                                              path:linkSocialPath
                                                            params:ssoInfo
                                                           queries:NULL
                                                 completionHandler:^(NSDictionary * _Nullable dataDictionary, SSOError * _Nullable error)
     {
         if (error != NULL)
         {
             failure(error);
             return ;
         }
         success();
     }];
    
    //                           } failure:^(SSOError * _Nullable error)
    //     {
    //         failure(error);
    //     }];
    //
}

-(void)delinkFacebook:(voidBlock)success
              failure:(errorBlock)failure
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setValue:SSOSOCIAL_TYPE_FACEBOOK forKey:SSOSOCIAL_OAUTH_SITEID_NEW_KEY];
    
    [[NSSODownloadManager ssoSharedManager] downloadDataForBaseUrl:SSOBaseUrl
                                                             scope:ssoBase
                                                              path:delinkSocialPath
                                                            params:params
                                                           queries:NULL
                                                 completionHandler:^(NSDictionary * _Nullable dataDictionary, SSOError * _Nullable error)
     {
         
         if (error != NULL)
         {
             [self removeSessionForUnauthorizedAccessCode:error.code];
             failure(error);
             return ;
         }
         success();
     }];
    
}

-(void)delinkGoogleplus:(voidBlock)success
                failure:(errorBlock)failure
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setValue:SSOSOCIAL_TYPE_FACEBOOK forKey:SSOSOCIAL_OAUTH_SITEID_NEW_KEY];
    [[NSSODownloadManager ssoSharedManager] downloadDataForBaseUrl:SSOBaseUrl
                                                             scope:ssoBase
                                                              path:delinkSocialPath
                                                            params:params
                                                           queries:NULL
                                                 completionHandler:^(NSDictionary * _Nullable dataDictionary, SSOError * _Nullable error)
     {
         
         if (error != NULL)
         {
             [self removeSessionForUnauthorizedAccessCode:error.code];
             failure(error);
             return ;
         }
         success();
     }];
}






#pragma mark  socialImageUpload

-(void)uploadProfilePicFromSocialUsingInfo:(NSDictionary *)info
                                   success:(voidBlock)success
                                   failure:(errorBlock)failure
{
    NSMutableDictionary *ssoInfo = [info mutableCopy];
    [ssoInfo  setValue:info[SSOSOCIAL_OAUTH_SITEID_KEY] forKey:SSOSOCIAL_OAUTH_SITEID_NEW_KEY];
    
    //    [self validateTruecallerParams:NULL
    //                     andFbGpParams:ssoInfo
    //                           success:^{
    [[NSSODownloadManager ssoSharedManager] downloadDataForBaseUrl:SSOBaseUrl
                                                             scope:ssoBase
                                                              path:socialImageUploadPath
                                                            params:ssoInfo
                                                           queries:NULL
                                                 completionHandler:^(NSDictionary * _Nullable dataDictionary, SSOError * _Nullable error)
     {
         
         
         if (error != NULL)
         {
             failure(error);
             return ;
         }
         success();
     }];
    
    //                           } failure:^(SSOError * _Nullable error) {
    //                               failure(error);
    //                           }];
}


//MARK:- Upload Profile Pic
UIViewController *pickerVC;
voidBlock voidSuccessBlock;
successBlock   imageUploadSuccess;
errorBlock          imageUploadFail;
imageUploadStart imageUploadStartBlock;
-(void)openPhotoSelectorOnViewController:(UIViewController *)vc
                                 success:(voidBlock)success
                                 failure:(errorBlock)failure
{
    pickerVC = vc;
    voidSuccessBlock = success;
    imageUploadFail = failure;
    
    [self pickPhotoOnViewController:vc];
}
- (void) resetPicUploadGlobalVariables
{
    pickerVC = nil;
    imageUploadSuccess = nil;
    imageUploadFail = nil;
    imageUploadStartBlock = nil;
    voidSuccessBlock = nil;
}

//MARK:- Utility methods: image picker
-(void)pickPhotoOnViewController:(UIViewController *)vc
{
    if ([UIAlertController class])
    {//iOS 8.0 and above
        UIAlertController * alert = [UIAlertController alertControllerWithTitle: @"UPLOAD PROFILE PICTURE" message: @"" preferredStyle: UIAlertControllerStyleAlert];
        [alert addAction: [UIAlertAction actionWithTitle: @"Photo Gallery    "
                                                   style: UIAlertActionStyleDefault
                                                 handler: ^(UIAlertAction * _Nonnull action)
                           {
                               //MARK: imagePickerController
                               UIImagePickerController * picker = [[UIImagePickerController alloc]init];
                               picker.delegate = self;
                               picker.allowsEditing = true;
                               picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                               //self->view.window!.rootViewController!
                               
                               [vc presentViewController: picker animated: true completion: ^{}];
                               
                           }]
         ];
        
        [alert addAction: [UIAlertAction actionWithTitle: @"Camera    "
                                                   style: UIAlertActionStyleDefault
                                                 handler: ^(UIAlertAction * _Nonnull action)
                           {
                               //MARK: imagePickerController
                               UIImagePickerController * picker = [[UIImagePickerController alloc]init];
                               picker.delegate = self;
                               picker.allowsEditing = true;
                               picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                               [vc presentViewController: picker animated: true completion: ^{}]; //self.view.window.rootViewController
                           }]
         ];
        
        //present image picker
        [vc presentViewController: alert
                         animated: true
                       completion: ^{
                           alert.view.superview.userInteractionEnabled = true;
                           [alert.view.superview addGestureRecognizer: [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(alertControllerBackgroundTapped:)]];
                       }];
    }
    else
    { //Below iOS 8.0
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle: @"UPLOAD PROFILE PICTURE"
                                                                 delegate: self
                                                        cancelButtonTitle: @"Cancel"
                                                   destructiveButtonTitle: nil
                                                        otherButtonTitles: @"Photo Gallery    ", @"Camera    ", nil];
        actionSheet.tag = 232;
        [actionSheet showInView: vc.view];
    }
}

-(void)actionSheet: (UIActionSheet *)actionSheet didDismissWithButtonIndex: (NSInteger)buttonIndex
{
    if (actionSheet.tag == 232)
    {
        if (![[NSString class] respondsToSelector: @selector(containsString)])
        {//ios 7 : containsString is available in iOS 8.0 and above
            if([[actionSheet buttonTitleAtIndex: buttonIndex] rangeOfString: @"Photo Gallery"].location != NSNotFound)
            {
                //Photo Library
                UIImagePickerController * picker = [[UIImagePickerController alloc]init];
                picker.delegate = self;
                picker.allowsEditing = true;
                picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                [pickerVC.view.window.rootViewController presentViewController: picker animated: true completion: ^{}];
                
            }
            else if([[actionSheet buttonTitleAtIndex: buttonIndex] rangeOfString: @"Camera"].location != NSNotFound)
            {
                //Camera
                UIImagePickerController * picker = [[UIImagePickerController alloc]init];
                picker.delegate = self;
                picker.allowsEditing = true;
                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                [pickerVC.view.window.rootViewController presentViewController: picker animated: true completion: ^{}];
                
            }
        }
        else  //for iOS 8
        {
            if([[actionSheet buttonTitleAtIndex: buttonIndex] containsString: @"Photo Gallery"])
            {
                //Photo Library
                UIImagePickerController * picker = [[UIImagePickerController alloc]init];
                picker.delegate = self;
                picker.allowsEditing = true;
                picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                [pickerVC.view.window.rootViewController presentViewController: picker animated: true completion: ^{}];
            }
            else if([[actionSheet buttonTitleAtIndex: buttonIndex] containsString: @"Camera"])
            {
                //Camera
                UIImagePickerController * picker = [[UIImagePickerController alloc]init];
                picker.delegate = self;
                picker.allowsEditing = true;
                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                [pickerVC.view.window.rootViewController presentViewController: picker animated: true completion: ^{}];
            }
        }
    }
}



-(void) alertControllerBackgroundTapped: (UITapGestureRecognizer *)tap
{
    [pickerVC dismissViewControllerAnimated: true completion: nil];
}

//UIImagePickerDelegate
- (void)imagePickerController: (UIImagePickerController *)picker didFinishPickingMediaWithInfo: (NSDictionary *)info
{
    UIImage * editedImage = info[UIImagePickerControllerEditedImage];
    
    [picker dismissViewControllerAnimated: true completion: ^{
        [self uploadRequest: editedImage];
    }];
}

//UploadRequest
-(NSString *) generateBoundaryString
{
    return [NSString stringWithFormat: @"Boundary-%@",[[NSUUID UUID] UUIDString]];
}

-(void) uploadRequest: (UIImage *)imageToSend
{
    NSData * image_data = UIImagePNGRepresentation(imageToSend);
    //  NSData * image_data = UIImageJPEGRepresentation(imageToSend,0.8);
    
    
    NSUInteger imageSize = image_data.length;
    NSUInteger imageSizeInKb = imageSize/1024;
    NSLog(@"size of image in KB: %lu", (unsigned long)imageSizeInKb);
    if (imageSizeInKb > 2048)
    {
        SSOError *error = [SSOError errorWithCode:SSOSDKErrorCodePicSizeExceeded
                                      description:[NSString stringWithFormat: @"Image size is %lu KB which is larger than we accept (2048 KB). Crop it to appropriate size and upload.",(unsigned long)imageSizeInKb]];
        
        if (imageUploadFail)
            imageUploadFail(error);
        
        [self resetPicUploadGlobalVariables];
        return;
    }
    
    UUID_BOUNDARY = [self generateBoundaryString];
    NSString * formBoundary = [NSString stringWithFormat: @"--%@\r\nContent-Disposition: form-data; name=",UUID_BOUNDARY];
    
    NSString * img_body = [NSString stringWithFormat: @"%@\"datafile\";filename=\"test.png\"\r\nContent-Type: image/png\r\n\r\n",formBoundary];
    NSString * tail_body = [NSString stringWithFormat: @"\r\n--%@--\r\n",UUID_BOUNDARY];
    NSMutableData * body_data = [[NSMutableData alloc]init];
    [body_data appendData: [img_body dataUsingEncoding: NSUTF8StringEncoding]];
    [body_data appendData: image_data];
    [body_data appendData: [tail_body dataUsingEncoding: NSUTF8StringEncoding]];
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible: YES];
    
    if (imageUploadStartBlock)
        imageUploadStartBlock();
    [[NSSODownloadManager ssoSharedManager] downloadDataForUrlImageUpload:SSOBaseUrl scope:ssoImageUpload path:uploadProfilePicPath andBody:body_data completionHandler:^(NSDictionary * _Nullable dataDictionary, SSOError * _Nullable error)
     {
         [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible: NO];
         if (error == nil)
         {
             if (imageUploadSuccess)
                 imageUploadSuccess(dataDictionary);
             if(voidSuccessBlock)
                 voidSuccessBlock(); //for deprecated 
             [self resetPicUploadGlobalVariables];
         }
         else
         {
             if (imageUploadFail)
                 imageUploadFail(error);
             
             [self resetPicUploadGlobalVariables];
         }
     }];

}

//Show Alert
-(void)showAlertWithTitle: (NSString *)title
             alertMessage: (NSString *)alert_msg
{
    [[[UIAlertView alloc] initWithTitle: title
                                message: alert_msg
                               delegate: self
                      cancelButtonTitle: @"OK"
                      otherButtonTitles: nil, nil]
     show];
}

//MARK:- Utility methods: validation and Error codes
-(NSUInteger)strLength:(NSString * )obj{
    if([obj isKindOfClass:[NSString class]]){
        return  [obj length];
    }
    return 0;
}

//-(void)validateTruecallerParams:(NSDictionary *)params
//                  andFbGpParams:(NSDictionary *)queries
//                        success:(voidBlock)success
//                        failure:(errorBlock)failure
//{
//    if (params.count>0)
//    {
//        if ([self strLength:[params valueForKey:SSOSOCIAL_PAYLOAD_KEY]]==0)
//        {
//
//            failure([SSOError errorWithCode:SSOSDKErrorCodePayloadNotFound]);
//            return;
//        }
//        if ([self strLength:[params valueForKey:SSOSOCIAL_SIGNATURE_KEY]]==0)
//        {
//            failure([SSOError errorWithCode:SSOSDKErrorCodeSignatureNotFound]);
//            return;
//        }
//        if ([self strLength:[params valueForKey:SSOSOCIAL_OAUTH_SITEID_KEY]]==0)
//        {
//            failure([SSOError errorWithCode:SSOSDKErrorCodeOauthSiteIdNotFound]);
//            return;
//        }
//    }
//    if (queries.count > 0)
//    {
//        if ([self strLength:[queries valueForKey:SSOSOCIAL_OAUTH_ID_KEY]]==0)
//        {
//            failure([SSOError errorWithCode:SSOSDKErrorCodeOauthIdNotFound]);
//            return;
//        }
//        if ([self strLength:[queries valueForKey:SSOSOCIAL_ACCESS_TOKEN_KEY]]==0)
//        {
//            failure([SSOError errorWithCode:SSOSDKErrorCodeAccessTokenNotFound]);
//            return;
//        }
//        if ([self strLength:[queries valueForKey:SSOSOCIAL_OAUTH_SITEID_KEY]]==0 ||
//            [self strLength:[queries valueForKey:SSOSOCIAL_OAUTH_SITEID_NEW_KEY]]==0)
//        {
//            failure([SSOError errorWithCode:SSOSDKErrorCodeOauthSiteIdNotFound]);
//            return;
//        }
//    }
//    success();
//}

-(BOOL)isSDKInitialized
{
    
    if (!appHeader.channel|| [appSession.tgId length]==0 ||!appHeader.siteId||!appHeader.teamId)
    {
        return false;
    }
    return true;
}
-(void)removeSessionForUnauthorizedAccessCode:(NSInteger)code
{
    if (code == SSOSDKErrorCodeUnauthorizedAccess)
    {
        [self removeSessions];
    }
}

-(void)saveSession:(SSOSession *)session option:(SSOSaveSessionOptions)option
{
    switch (option)
    {
        case SSODefaultSession:
            [appSession initializeAppSessionWithSession:session];
            [globalSession initializeSharedSessionWithSession:session];
            break;
            
        case SSOCopiedSession:
            [appSession initializeAppSessionWithSession:session];
            break;
            
        case SSOMigratedAppOnlySession:
            [appSession initializeAppSessionWithSession:session];
            break;
        default:
            break;
    }
}
-(void) removeGlobalSession
{
    [globalSession removeSharedSessionSsec];
    [globalSession removeSharedSessionTicketId];
    [globalSession removeSharedSessionType];
    [globalSession removeSharedSessionIdentifier];
}

//MARK:- Utility methods: get tgid
-(void)gettgidOnCompletion:(completionBlock)completion
{
    NSString *deviceid = [UIDevice currentDevice].identifierForVendor.UUIDString;
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setValue:appHeader.channel forKey:SSOHEADER_CHANNEL_KAY];
    [params setValue:deviceid forKey:SSO_DEVICEiD_KEY];
    
    [[NSSODownloadManager ssoSharedManager] downloadDataForBaseUrl:SSOBaseUrl scope:ssomSocial path:getDataForDeviceUrlPath params:NULL queries:params completionHandler:^(NSDictionary * _Nullable dataDictionary, SSOError * _Nullable error)
     {
         if (error != NULL)
         {
             completion(NULL,error);
             return ;
         }
         
         SSOSession *k_session = [[SSOSession alloc] initWithDictionary:dataDictionary];
         [globalSession initializeGlobalSessionWithTgId:k_session.tgId];
         [appSession initializeAppSessionWithTgId:k_session.tgId];

         completion(NULL,NULL);
     }];
}
//MARK:- Utility methods: remove sessions
-(void)removeSessions
{
    if ([appSession.ssec length] == 0)
    {
        return; //already logout
    }
    if ([globalSession.ssec isEqualToString:appSession.ssec])
    {
        [self removeGlobalSession];
    }
    
    [appSession removeAppSessionSsec];
    [appSession removeAppSessionTicketId];
    
}

//MARK:- Utility methods: Get new Ticket
-(void)getNewTicketOnCompletion:(sessionBlock)completion
{
    [[NSSODownloadManager ssoSharedManager] downloadDataForBaseUrl:SSOBaseUrl
                                                             scope:ssoBase
                                                              path:getNewTicket
                                                            params:NULL
                                                           queries:NULL
                                                 completionHandler:^(NSDictionary * _Nullable dataDictionary, SSOError * _Nullable error)
     {
         
         if (error != NULL)
         {
             [self removeSessionForUnauthorizedAccessCode:error.code];
             completion(NULL,error);
             return ;
         }
         SSOSession * session = [[SSOSession alloc] initWithDictionary:dataDictionary];
         completion(session,NULL);
     }];
    
}
//MARK:- Utility methods: socialInfoParams
-(NSMutableDictionary *)paramsForSocialInfo:(NSDictionary *)info
{
    NSMutableDictionary *ssoInfo;
    
    if (![info isKindOfClass:[NSMutableDictionary class]])
    {
        ssoInfo = [info mutableCopy];
    }
    else
    {
        ssoInfo = (NSMutableDictionary *)info;
    }
    [ssoInfo setValue:info[SSOSOCIAL_OAUTH_SITEID_KEY] forKey:SSOSOCIAL_OAUTH_SITEID_NEW_KEY];
    ssoInfo = [self setExtraParamsForSocialInfo:ssoInfo];
    return ssoInfo;
}
#pragma mark - New Apis 2.2

-(void)initializeSDKOnSuccess:(voidBlock)success
                      failure:(errorBlock)failure
{
    appHeader = [[SSOHeaders alloc] init];
    appSession = [[SSOSession alloc] initWithAppKeychain];
    globalSession = [[SSOSession alloc] initWithSharedKeychain];
    if ([appSession.tgId length] != 0)
    {
        success();
        return;
    }
    if ([globalSession.tgId length] != 0)
    {
        [appSession initializeAppSessionWithTgId:globalSession.tgId];
        success();
        return;
    }
    
    [self gettgidOnCompletion:^(NSDictionary *info, SSOError *error)
     {
         if (error)
         {
             failure(error);
         }
         else
         {
             success();
         }
     }];
    
}

-(void)performSocialActivity:(SSOSocialActivityOptions)option
             usingSocialInfo:(SSOSocialInfo *)info
                     success:(voidBlock)success
                     failure:(errorBlock)failure
{
    if (info != nil && ![info isKindOfClass:SSOSocialInfo.class])
    {
        NSAssert(NO, @"Plesae send details in object of type : SSOSocialInfo");
    }
    NSString *baseUrl = SSOBaseUrl;
    NSString *path = @"";
    NSUInteger scope = ssoBase;
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *queries = [[NSMutableDictionary alloc] init];
    
    switch (option)
    {
        case SSOFacebookLogin:
            baseUrl = SSOmSocialBaseUrl;
            path = signInWithFacebookResponse;
            scope = ssomSocial;
            queries = [self getLoginParamsFromSocialInfo:info];
            [queries setValue:SSOSOCIAL_TYPE_FACEBOOK forKey:SSOSOCIAL_OAUTH_SITEID_NEW_KEY];
            break;
            
        case SSOGoogleLogin:
            baseUrl = SSOmSocialBaseUrl;
            path = signInWithGoogleResponse;
            scope = ssomSocial;
            queries = [self getLoginParamsFromSocialInfo:info];
            [queries setValue:SSOSOCIAL_TYPE_GOOGLEPLUS forKey:SSOSOCIAL_OAUTH_SITEID_NEW_KEY];
            break;
            
        case SSOTruecallerLogin:
            path = truecallerLoginPath;
            params = [self getTruecallerParamsFromSocialInfo:info];
            [params setValue:SSOSOCIAL_TYPE_TRUECALLER forKey:SSOSOCIAL_OAUTH_SITEID_KEY];
            break;
            
        case SSOFacebookLink:
            path = linkSocialPath;
            params = [self getFbGpParamsFromSocialInfo:info];
            [params setValue:SSOSOCIAL_TYPE_FACEBOOK forKey:SSOSOCIAL_OAUTH_SITEID_NEW_KEY];
            break;
            
        case SSOGoogleLink:
            path = linkSocialPath;
            params = [self getFbGpParamsFromSocialInfo:info];
            [params setValue:SSOSOCIAL_TYPE_GOOGLEPLUS forKey:SSOSOCIAL_OAUTH_SITEID_NEW_KEY];
            break;
            
        case    SSOFacebookPicUpload:
            path = socialImageUploadPath;
            params = [self getFbGpParamsFromSocialInfo:info];
            [params setValue:SSOSOCIAL_TYPE_FACEBOOK forKey:SSOSOCIAL_OAUTH_SITEID_NEW_KEY];
            break;
            
        case   SSOGooglePicUpload:
            path = socialImageUploadPath;
            params = [self getFbGpParamsFromSocialInfo:info];
            [params setValue:SSOSOCIAL_TYPE_GOOGLEPLUS forKey:SSOSOCIAL_OAUTH_SITEID_NEW_KEY];
            break;
        case SSOFacebookDelink:
            path = delinkSocialPath;
            [params setValue:SSOSOCIAL_TYPE_FACEBOOK forKey:SSOSOCIAL_OAUTH_SITEID_NEW_KEY];
            break;
        case SSOGoogleDelink:
            path =  delinkSocialPath;
            [params setValue:SSOSOCIAL_TYPE_GOOGLEPLUS forKey:SSOSOCIAL_OAUTH_SITEID_NEW_KEY];
            break;
        default:
            break;
    }
    [[NSSODownloadManager ssoSharedManager] downloadDataForBaseUrl:baseUrl
                                                             scope:scope
                                                              path:path
                                                            params:params
                                                           queries:queries
                                                 completionHandler:^(NSDictionary * _Nullable dataDictionary, SSOError * _Nullable error)
     {
         if (error != NULL)
         {
             switch (option) {
                 case SSOFacebookLink:
                 case SSOGoogleLink:
                 case SSOFacebookDelink:
                 case SSOGoogleDelink:
                 case SSOFacebookPicUpload:
                 case SSOGooglePicUpload:
                     [self removeSessionForUnauthorizedAccessCode:error.code];
                     
                     break;
                     
                 default:
                     break;
             }
             failure(error);
             return ;
         }
         
         SSOSession *k_session = [[SSOSession alloc] initWithDictionary:dataDictionary];
         
         switch (option)
         {
             case SSOFacebookLogin:
                 
                 k_session.type = SSOSOCIAL_TYPE_FACEBOOK;
                 [self saveSession:k_session option:SSODefaultSession];
                 break;
                 
             case SSOGoogleLogin:
                 k_session.type = SSOSOCIAL_TYPE_GOOGLEPLUS;
                 [self saveSession:k_session option:SSODefaultSession];
                 break;
                 
             case SSOTruecallerLogin:
                 k_session.type = SSOSOCIAL_TYPE_TRUECALLER;
                 [self saveSession:k_session option:SSODefaultSession];
                 break;
                 
             default:
                 break;
         }//end of Switch
         success();
     }];
    
}


-(void)updateUserDetails:(SSOUserUpdates *)userDetails
                 success:(userUpdatesBlock)success
                 failure:(errorBlock)failure
{
    if (userDetails != nil && ![userDetails isKindOfClass:SSOUserUpdates.class])
    {
        NSAssert(NO, @"Plesae send details in object of type : SSOUserUpdates");
    }
    
    NSDictionary *info = [self dictionaryWithPropertiesOfObject:userDetails];
    
    [[NSSODownloadManager ssoSharedManager] downloadDataForBaseUrl:SSOBaseUrl
                                                             scope:ssoBase
                                                              path:updateUserDetailsPath
                                                            params:info
                                                           queries:NULL
                                                 completionHandler:^(NSDictionary * _Nullable dataDictionary, SSOError * _Nullable error)
     {
         
         
         
         if (error != NULL)
         {
             
             [self removeSessionForUnauthorizedAccessCode:error.code];
             failure(error);
             return ;
         }
         SSOUserUpdates *u = [[SSOUserUpdates alloc] initWithDictionary:dataDictionary];
         success(u);
     }];
}

-(void)performSignupActivity:(SSOSignupOptions)options
                     forUser:(SSOSignupUser *)user
                     success:(voidBlock)success
                     failure:(errorBlock)failure
{
    if (user != nil && ![user isKindOfClass:SSOSignupUser.class])
    {
        NSAssert(NO, @"Plesae send details in object of type : SSOSignupUser");
    }
    
    NSString *path = @"";
    
    NSDictionary *params = [self dictionaryWithPropertiesOfObject:user];
    switch (options)
    {
        case SSOFullSignup:
            path = registerUserAPIPath;
            
            break;
        case SSOOnlyMobileSignup:
            path = registerOnlyMobilePath;
            break;
            
        default:
            break;
    }
    
    [[NSSODownloadManager ssoSharedManager] downloadDataForBaseUrl:SSOBaseUrl
                                                             scope:ssoBase
                                                              path:path
                                                            params:params
                                                           queries:NULL
                                                 completionHandler:^(NSDictionary * _Nullable dataDictionary, SSOError * _Nullable error)
     {
         
         
         if (error != NULL)
         {
             failure(error);
             return ;
         }
         SSOSignupUser *u = [[SSOSignupUser alloc] initWithDictionary:dataDictionary];
         ssoid = u.ssoid;
         // Signup otp sent
         success();
     }];
}


-(void)performPickUploadActivity:(SSOPickUploadOptions)options
                    onController:(UIViewController *)vc
                    startRequest:(imageUploadStart) uploadStart
                         success:(successBlock)success
                         failure:(errorBlock)failure
{
    
    UIImagePickerController * picker = [[UIImagePickerController alloc]init];
    picker.delegate = self;
    picker.allowsEditing = true;
    
    

    imageUploadSuccess = success;
    imageUploadFail = failure;
    imageUploadStartBlock = uploadStart;
    

    switch (options)
    {
        case SSOCamera:
            if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
            {
                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                [vc presentViewController: picker animated: true completion: ^{}];
            }
            break;
        case SSOPhotoGallery:
            
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [vc presentViewController: picker animated: true completion: ^{}];
            
            break;
            
        default:
            pickerVC = vc;
            [self pickPhotoOnViewController:vc];
            break;
    }
    
}


-(void) createAppSessionForTicketId:(NSString *)ticketId completion:(completionBlock)completion
{
    if (![self isSDKInitialized])
    {
        completion(NULL,[SSOError errorWithCode:SSOSDKErrorCodeSDKNotInitialized]);
        return;
    }
    if ([ticketId isKindOfClass:[NSString class]])
    {
        MIGRATE_TICKETID = ticketId;
    }
    
    [[NSSODownloadManager ssoSharedManager] downloadDataForBaseUrl:SSOBaseUrl
                                                             scope:ssoBase
                                                              path:getSsecFromTicket
                                                            params:NULL
                                                           queries:NULL
                                                 completionHandler:^(NSDictionary * _Nullable dataDictionary, SSOError * _Nullable error)
     {
         if (error != NULL)
         {
             completion(NULL,error);
             return ;
         }
         
         SSOSession *k_session = [[SSOSession alloc] initWithDictionary:dataDictionary];
         [self saveSession:k_session option:SSOMigratedAppOnlySession];
         completion(NULL,NULL);
     }];
    
}



-(NSMutableDictionary *)getFbGpParamsFromSocialInfo:(SSOSocialInfo *)info
{
    NSMutableDictionary *ssoInfo = [[NSMutableDictionary alloc] init];
    [ssoInfo setValue:info.oauthId forKey: SSOSOCIAL_OAUTH_ID_KEY];
    [ssoInfo setValue:info.accessToken forKey:SSOSOCIAL_ACCESS_TOKEN_KEY];
    return ssoInfo;
}

-(NSMutableDictionary *)getLoginParamsFromSocialInfo:(SSOSocialInfo *)info
{
    NSMutableDictionary *ssoInfo = [self getFbGpParamsFromSocialInfo:info];
    ssoInfo = [self setExtraParamsForSocialInfo:ssoInfo];
    return ssoInfo;
}

-(NSMutableDictionary *)setExtraParamsForSocialInfo:(NSMutableDictionary *)ssoInfo
{
    [ssoInfo setValue:appHeader.siteId forKey:SSOHEADER_SITE_ID_KAY];
    [ssoInfo setValue:[UIDevice currentDevice].identifierForVendor.UUIDString forKey:SSO_DEVICEID_KEY];
    [ssoInfo setValue:@"yes" forKey:SSO_SSEC_REQ_KEY];
    [ssoInfo setValue:appHeader.channel forKey:SSOHEADER_CHANNEL_KAY];
    [ssoInfo setValue:appHeader.channel forKey:SSO_SITE_REG_KEY];
    return ssoInfo;
    
}
-(NSMutableDictionary *)getTruecallerParamsFromSocialInfo:(SSOSocialInfo *)info
{
    NSMutableDictionary *ssoInfo = [[NSMutableDictionary alloc] init];
    [ssoInfo setValue:info.payload forKey:SSOSOCIAL_PAYLOAD_KEY];
    [ssoInfo setValue:info.signature forKey:SSOSOCIAL_SIGNATURE_KEY];
    return ssoInfo;
}

- (NSDictionary *) dictionaryWithPropertiesOfObject:(id)obj
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    unsigned count;
    objc_property_t *properties = class_copyPropertyList([obj class], &count);
    
    for (int i = 0; i < count; i++) {
        NSString *key = [NSString stringWithUTF8String:property_getName(properties[i])];
        id value = [obj valueForKey:key];
        if ([value isKindOfClass:NSString.class] && [value isEqualToString:@""])
        {
            value = nil;
        }
        [dict setValue:value forKey:key]; 
    }
    
    free(properties);
    
    return [NSDictionary dictionaryWithDictionary:dict];
}

@end
