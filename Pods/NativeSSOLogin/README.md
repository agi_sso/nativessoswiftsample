# NativeSSOLogin

[![CI Status](http://img.shields.io/travis/Pankaj Verma/NativeSSOLogin.svg?style=flat)](https://travis-ci.org/Pankaj Verma/NativeSSOLogin)
[![Version](https://img.shields.io/cocoapods/v/NativeSSOLogin.svg?style=flat)](http://cocoapods.org/pods/NativeSSOLogin)
[![License](https://img.shields.io/cocoapods/l/NativeSSOLogin.svg?style=flat)](http://cocoapods.org/pods/NativeSSOLogin)
[![Platform](https://img.shields.io/cocoapods/p/NativeSSOLogin.svg?style=flat)](http://cocoapods.org/pods/NativeSSOLogin)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

NativeSSOLogin is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "NativeSSOLogin"
```

## Author

Pankaj Verma, pankaj.verma@timesinternet.in

## License

NativeSSOLogin is available under the MIT license. See the LICENSE file for more info.
